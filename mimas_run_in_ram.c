/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "nrf_drv_wdt.h"
#include "mimas_run_in_ram.h"
#include "nrf_soc.h"
#include "app_scheduler.h"
#include "ble_advdata.h"

//#define ADXL362_WORK_LED_ON
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
extern uint8_t m_beacon_info[];
extern uint16_t global_connect_handle;
extern int8_t g_device_tx_power;
extern void leda_wdt_feed(void);
extern void mimas_sleep_mode_enter(void);
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
/**@brief Function for the Power manager.
 */
static void power_manage(void)
{
    //LOG_ENTER;
    uint32_t err_code = sd_app_evt_wait();
    APP_ERROR_CHECK(err_code);
}

void mimas_loop(void)
{
    for (;;)
    {
#ifdef LEDA_WDT
        leda_wdt_feed();
#endif
        //LOG("fr%d\t", ti++);
        //LEDS_ON(LEDS_MASK);
        app_sched_execute();
#ifdef LEDA_WDT
        leda_wdt_feed();
#endif
        power_manage();
#ifdef LEDA_WDT
        leda_wdt_feed();
#endif
        //LEDS_INVERT(LEDS_MASK);
        //LEDS_OFF(LEDS_MASK);
    }
}

#define NON_CONNECTABLE_ADV_INTERVAL    MSEC_TO_UNITS(100, UNIT_0_625_MS) /**< The advertising interval for non-connectable advertisement (100 ms). This value can vary between 100ms to 10.24s). */
void advertising_init_beacon(void)
{
    //int8_t        tx_power_level = TX_POWER_LEVEL;
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t rspdata;//scan response data
    uint8_t       flags = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;

    ble_advdata_manuf_data_t manuf_specific_data;
    manuf_specific_data.company_identifier = APP_COMPANY_IDENTIFIER;
    manuf_specific_data.data.p_data = (uint8_t *) m_beacon_info;
    manuf_specific_data.data.size   = APP_BEACON_INFO_LENGTH;

    // Build and set advertising data.
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type             = BLE_ADVDATA_NO_NAME;
    advdata.flags                 = flags;
    //advdata.p_tx_power_level      = &tx_power_level;
    //advdata.include_appearance    = true;
    advdata.p_manuf_specific_data = &manuf_specific_data;

//    m_advertising_mode = BLE_FOREVER;

    memset(&rspdata, 0, sizeof(rspdata));
    rspdata.name_type = BLE_ADVDATA_FULL_NAME;
    rspdata.p_tx_power_level = &g_device_tx_power;
    
    err_code = ble_advdata_set(&advdata, &rspdata);
    APP_ERROR_CHECK(err_code);
#if 0
    // Initialize advertising parameters (used when starting advertising).
    memset(&m_adv_params, 0, sizeof(m_adv_params));

    //m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_NONCONN_IND;
    m_adv_params.type        = BLE_GAP_ADV_TYPE_ADV_IND;
    m_adv_params.p_peer_addr = NULL;                             // Undirected advertisement.
    m_adv_params.fp          = BLE_GAP_ADV_FP_ANY;
    m_adv_params.interval    = NON_CONNECTABLE_ADV_INTERVAL;
    m_adv_params.timeout     = 0;
#endif
}

extern uint16_t g_battery_voltage_mv;
extern uint8_t g_percentage_batt_lvl;
void advertising_init_microduino(void)
{
    uint32_t      err_code;
    ble_advdata_t advdata;
    ble_advdata_t rspdata;//scan response data
    //int8_t        tx_power_level = TX_POWER_LEVEL;

    ble_uuid_t adv_uuids[4] =
    {
        {0xFFF0, BLE_UUID_TYPE_BLE},
        {0xFFE0, BLE_UUID_TYPE_BLE},
        {0x7677, BLE_UUID_TYPE_BLE},
        {0x7879, BLE_UUID_TYPE_BLE},
    };
    adv_uuids[2].uuid = g_battery_voltage_mv;
    adv_uuids[3].uuid = g_percentage_batt_lvl;

    //m_advertising_mode = BLE_NO_ADV;

    // Build and set advertising data
    memset(&advdata, 0, sizeof(advdata));

    advdata.name_type               = BLE_ADVDATA_NO_NAME;
    advdata.include_appearance      = true;
    advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    //advdata.p_tx_power_level        = &tx_power_level;
    advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    advdata.uuids_complete.p_uuids  = adv_uuids;

    memset(&rspdata, 0, sizeof(rspdata));
    rspdata.name_type = BLE_ADVDATA_FULL_NAME;
    rspdata.p_tx_power_level = &g_device_tx_power;
    
    err_code = ble_advdata_set(&advdata, &rspdata);
    APP_ERROR_CHECK(err_code);
}
