
#ifndef __LEDA_AVR_H__
#define __LEDA_AVR_H__

#include "stdint.h"
#include "mimas_ble_nus.h"

/******************* Macro defination *************************************/

/*****************************************/

/******************* Function defination **********************************/
extern void init_avr(void);
extern void leda_update_avr_pre(void);
extern void update_avr_continue(void);
extern uint32_t avr_uart_init(uint32_t baudrate);
extern void avr_uart_uninit(void);
extern void leda_init_update_FSM(void);
extern void leda_stop_update_avr(void);
#endif //__LEDA_AVR_H__

