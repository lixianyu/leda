
#include "leda_queue.h"
#include "leda_communicate_protocol.h"
#include "tethys_led.h"

#if 0
bool Leda_FullQueue(void)
{
    if(gQUEUE.front == (gQUEUE.rear + 1) % gQUEUE.maxsize) //?????????,?????????
        return true;
    else
        return false;
}

bool Leda_Enqueue(uint8_t val)
{
    if(Leda_FullQueue())
        return false;
    else
    {
        gQUEUE.pBase[gQUEUE.rear] = val;
        gQUEUE.rear = (gQUEUE.rear + 1) % gQUEUE.maxsize;
        return true;
    }
}

bool Leda_Dequeue(uint8_t *val)
{
    if(Leda_EmptyQueue())
    {
        return false;
    }
    else
    {
        *val = gQUEUE.pBase[gQUEUE.front];
        gQUEUE.front = (gQUEUE.front + 1) % gQUEUE.maxsize;
        return true;
    }
}
#endif

static uint8_t gQueue[LEDA_QUEUE_SIZE];
static QUEUE gQUEUE;
void Leda_InitQueue(void)
{
    gQUEUE.pBase = &gQueue[0];
    gQUEUE.front = 0;
    gQUEUE.rear = 0;
    gQUEUE.maxsize = LEDA_QUEUE_SIZE;
    gQUEUE.count = 0;
}

uint8_t *Leda_GetCurrent(uint8_t *pLen)
{
    if (gQUEUE.count < MD_BLOCK_SIZE)
    {
        *pLen = gQUEUE.count;
    }
    else
    {
        *pLen = MD_BLOCK_SIZE;
    }
    return &gQUEUE.pBase[gQUEUE.front];
}

bool Leda_EmptyQueue(void)
{
    if(gQUEUE.count == 0)
        return true;
    else
        return false;
}

uint8_t *Leda_EnqueueArray(uint8_t *val, uint8_t len)
{
    uint8_t *pBegin = &gQUEUE.pBase[gQUEUE.rear];
    memcpy(pBegin, val, len);
    gQUEUE.rear = (gQUEUE.rear + len) % gQUEUE.maxsize;
    gQUEUE.count += len;
    if (gQUEUE.count >= LEDA_QUEUE_SIZE)
    {
        open_LED(3);
        open_LED(4);
    }
    return pBegin;
}

bool Leda_DequeueArray(uint8_t len)
{
    gQUEUE.front = (gQUEUE.front + len) % gQUEUE.maxsize;
    gQUEUE.count -= len;
    if (gQUEUE.count <= 0)
    {
        return false;
    }
    return true;
}

void Leda_DequeueBuf(uint8_t *val, uint8_t len)
{
    if(gQUEUE.count == 0)
    {
        return;
    }
    memcpy(val, &gQUEUE.pBase[gQUEUE.front], len);
    gQUEUE.front = (gQUEUE.front + len) % gQUEUE.maxsize;
    gQUEUE.count -= len;
}

