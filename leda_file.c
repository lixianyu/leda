#include <stdlib.h>
#include "pstorage.h"
#include "leda_file.h"
#include "app_timer.h"
#include "mimas_config.h"
#include "mimas_log.h"
#include "tethys_led.h"
#include "leda_communicate_protocol.h"
#include "leda_queue.h"

extern uint32_t gBaudrate;
extern bool gLedaIsUpdattingAVR;

typedef enum
{
    TETHYS_PSTORAGE_NOTHING,
    TETHYS_PSTORAGE_UPDATE_TEMP_PRE,
    TETHYS_PSTORAGE_,
} TETHYS_PSTORAGE_STATE_t;

TETHYS_PSTORAGE_STATE_t g_Tethys_pstorage_state = TETHYS_PSTORAGE_NOTHING;
static uint32_t g_StorageBegin = 0;
static uint32_t g_AddressBegin = 0;// Offset

static uint32_t g_StorageActive = 0;
static uint32_t g_AddressActive = 0;// Offset
static uint32_t g_StorageActiveForRead = 0;
static uint32_t g_AddressActiveForRead = 0;// Offset

//static uint16_t gBlockNumberOpenLockTimes = 0;
static uint16_t gOffsetNumberLxy = 0;

uint32_t g_bin_size = 0;

static uint8_t gStorageClear = 0;
static bool gIfWriteFinish = false;
bool gUseRAMUpdateAVR = false;
uint32_t gTimesBLEAVR = 0; //通过蓝牙升级AVR的次数
uint32_t gTemperatureOffsetCount = 0;
APP_TIMER_DEF(m_file_timer_id);
static void tethys_pstorage_callback_handler1(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len);
static void pstorage_callback_handler(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len);
static void pstorage_callback_handler_lxy1(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len);
static void pstorage_callback_handler_temp(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len);
static void elara_flash_erase_next(void);
#if 0
pstorage_handle_t g_storage_lock_handle1;
pstorage_handle_t g_storage_lock_handle2;
pstorage_handle_t g_storage_lock_handle3;
pstorage_handle_t g_storage_lock_handle4;
pstorage_handle_t g_storage_lock_handle5;
pstorage_handle_t g_storage_lock_handle6;
pstorage_handle_t g_storage_lock_handle7;
pstorage_handle_t g_storage_lock_handle8;
pstorage_handle_t g_storage_lock_handle9;
pstorage_handle_t g_storage_lock_handle10;
pstorage_handle_t g_storage_lock_handle11;
pstorage_handle_t g_storage_lock_handle12;
pstorage_handle_t g_storage_lock_handle13;
pstorage_handle_t g_storage_lock_handle14;
pstorage_handle_t g_storage_lock_handle15;
pstorage_handle_t g_storage_lock_handle16;
pstorage_handle_t g_storage_lock_handle17;
pstorage_handle_t g_storage_lock_handle18;
pstorage_handle_t g_storage_lock_handle19;
pstorage_handle_t g_storage_lock_handle20;
pstorage_handle_t g_storage_lock_handle21;
pstorage_handle_t g_storage_lock_handle22;
pstorage_handle_t g_storage_lock_handle23;
pstorage_handle_t g_storage_lock_handle24;
pstorage_handle_t g_storage_lock_handle25;
pstorage_handle_t g_storage_lock_handle26;
pstorage_handle_t g_storage_lock_handle27;
pstorage_handle_t g_storage_lock_handle28;
pstorage_handle_t g_storage_lock_handle29;
pstorage_handle_t g_storage_lock_handle30;
pstorage_handle_t g_storage_lock_handle31;
pstorage_handle_t g_storage_lock_handle32;
#else
static pstorage_handle_t g_storage_leda_handle[32];
#endif
static pstorage_handle_t g_storage_handle_lxy;
static pstorage_handle_t g_storage_handle_lxy1;
static pstorage_handle_t g_storage_handle_temperature;
static int32_t g_adcT, g_curTOffset;

static void leda_init_storage_lxy(void)
{
    pstorage_handle_t block_handle;
    uint16_t offsetNB;
    pstorage_block_identifier_get(&g_storage_handle_lxy, 0, &block_handle);
    for (offsetNB = 0; offsetNB < 1024; offsetNB += 8)
    {
        pstorage_load((uint8_t *)&g_StorageActive, &block_handle, 4, offsetNB);
        if (g_StorageActive == 0xFFFFFFFF)
        {
            goto OHLeaveMe;
        }
    }
OHLeaveMe:
    gOffsetNumberLxy = offsetNB;
    if (offsetNB == 0)
    {
        //open_LED(3);
        //open_LED(4);
        g_StorageActive = 0;
        g_AddressActive = 0;
    }
    else
    {
        //open_LED(3);
        offsetNB -= 8;
        //pstorage_block_identifier_get(&g_storage_handle_lxy, 0, &block_handle);
        pstorage_load((uint8_t *)&g_StorageActive, &block_handle, 4, offsetNB);
        pstorage_load((uint8_t *)&g_AddressActive, &block_handle, 4, offsetNB + 4);
    }
}

static void file_timeout_handler(void *p_context)
{
    elara_flash_erase_next();
}

int32_t leda_storage_get_temp_offset(int32_t adcTemp)
{
    pstorage_handle_t block_handle;
    bool flagIfGot = false;
    int32_t adcT;
    uint32_t i,iEnd;
    int32_t subValue = 0;
    int32_t subValueMin = 0;
    bool ifFirst = true;
    int32_t offsetClosest = 0;
    pstorage_block_identifier_get(&g_storage_handle_temperature, 0, &block_handle);
    iEnd = gTemperatureOffsetCount * 8;
    for (i = 0; i < iEnd; i += 8)
    {
        pstorage_load((uint8_t*)&adcT, &block_handle, 4, i);
        if (adcTemp == adcT)
        {
            flagIfGot = true;
            break;
        }
        if (ifFirst)
        {
            ifFirst = false;
            subValueMin = abs(adcT - adcTemp);
            pstorage_load((uint8_t*)&offsetClosest, &block_handle, 4, i + 4);
        }
        else
        {
            subValue = abs(adcT - adcTemp);
            if (subValue < subValueMin)
            {
                subValueMin = subValue;
                pstorage_load((uint8_t*)&offsetClosest, &block_handle, 4, i + 4);
            }
        }
    }
    if (flagIfGot)
    {
        pstorage_load((uint8_t*)&offsetClosest, &block_handle, 4, i + 4);
    }
    return offsetClosest;
}

/*
 * adcTemp: 由ADC得到的温度
 * curTempOffset : 当前环境实际温度
 */
void leda_storage_update_temp_offset(int32_t adcTemp, int32_t curTemp)
{
    pstorage_handle_t block_handle;
    bool flagIfUpdate = false;
    int32_t adcT;
    uint32_t i,iEnd;
    int32_t curTempOffset = curTemp - adcTemp;
        
    pstorage_block_identifier_get(&g_storage_handle_temperature, 0, &block_handle);
    iEnd = gTemperatureOffsetCount * 8;
    for (i = 0; i < iEnd; i += 8)
    {
        pstorage_load((uint8_t*)&adcT, &block_handle, 4, i);
        //pstorage_load((uint8_t*)&curT, &block_handle, 4, i + 4);
        if (adcT == adcTemp)
        {
            flagIfUpdate = true;
            g_adcT = adcTemp;
            g_curTOffset = curTempOffset;
            break;
        }
    }
    if (flagIfUpdate)
    {
        pstorage_update(&block_handle, (uint8_t*)&g_adcT, 4, i);
        pstorage_update(&block_handle, (uint8_t*)&g_curTOffset, 4, i + 4);
    }
    else
    {
        g_adcT = adcTemp;
        g_curTOffset = curTempOffset;
        pstorage_store(&block_handle, (uint8_t*)&g_adcT, 4, iEnd);
        pstorage_store(&block_handle, (uint8_t*)&g_curTOffset, 4, iEnd + 4);
        pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_TEMP_OFFSET_COUNT, &block_handle);
        gTemperatureOffsetCount++;
        pstorage_update(&block_handle, (uint8_t*)&gTemperatureOffsetCount, 4, 0);
    }
}

static void leda_init_storage_temperature(void)
{
    uint32_t err_code;
    pstorage_module_param_t param;
    param.block_size  = 1024;//4
    param.block_count = 1;
    param.cb          = pstorage_callback_handler_temp;
    err_code = pstorage_register(&param, &g_storage_handle_temperature);
    APP_ERROR_CHECK(err_code);
}

static void leda_init_storage_lxy1(void)
{
    uint32_t err_code;
    pstorage_module_param_t param;
    param.block_size  = BLOCK_SIZE;//4
    param.block_count = BLOCK_COUNT;
    param.cb          = pstorage_callback_handler_lxy1;
    err_code = pstorage_register(&param, &g_storage_handle_lxy1);
    APP_ERROR_CHECK(err_code);

    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_BAUDRATE, &block_handle);
    uint32_t baudrate;
    pstorage_load((uint8_t*)&baudrate, &block_handle, 4, 0);
    if (baudrate != 0xFFFFFFFF)
    {
        gBaudrate = baudrate;
    }
    ///////////////////////////////////////////////////////////////////////////////////////
    pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_BLEAVR, &block_handle);
    pstorage_load((uint8_t*)&gTimesBLEAVR, &block_handle, 4, 0);
    if (gTimesBLEAVR == 0xFFFFFFFF)
    {
        gTimesBLEAVR = 0;
    }
    ///////////////////////////////////////////////////////////////////////////////////////
    pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_TEMP_OFFSET_COUNT, &block_handle);
    pstorage_load((uint8_t*)&gTemperatureOffsetCount, &block_handle, 4, 0);
    if (gTemperatureOffsetCount == 0xFFFFFFFF)
    {
        gTemperatureOffsetCount = 0;
    }
}

void leda_storage_update_baudrate(void)
{
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_BAUDRATE, &block_handle);
    pstorage_update(&block_handle, (uint8_t*)&gBaudrate, 4, 0);
}

void leda_storage_update_BLEAVR(void)
{
    gTimesBLEAVR++;
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_lxy1, BLOCK_NUM_BLEAVR, &block_handle);
    pstorage_update(&block_handle, (uint8_t*)&gTimesBLEAVR, 4, 0);
}

void leda_pstorage_init(void)
{
    uint32_t err_code;
    pstorage_module_param_t param;

    param.block_size  = 1024;
    param.block_count = 1;
    param.cb = tethys_pstorage_callback_handler1;
    for (int i = 0; i < 32; i++)
    {
        err_code = pstorage_register(&param, &g_storage_leda_handle[i]);
        APP_ERROR_CHECK(err_code);
    }

    param.block_size  = 1024;
    param.block_count = 1;
    param.cb          = pstorage_callback_handler;
    err_code = pstorage_register(&param, &g_storage_handle_lxy);
    APP_ERROR_CHECK(err_code);
    leda_init_storage_temperature();
    leda_init_storage_lxy1();
    leda_init_storage_lxy();
  
    app_timer_create(&m_file_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     file_timeout_handler);
}

void elara_flash_write_init(void)
{
    uint16_t offsetNB;
    pstorage_handle_t block_handle;

    Leda_InitQueue();
    if (gUseRAMUpdateAVR)
    {
        gIfWriteFinish = true;
    }
    else
    {
        gIfWriteFinish = false;
        if (gOffsetNumberLxy == 0)
        {
            g_StorageBegin = g_StorageActive;
            g_AddressBegin = g_AddressActive;
            g_bin_size = 0;
            return;
        }
        pstorage_block_identifier_get(&g_storage_handle_lxy, 0, &block_handle);
        offsetNB = gOffsetNumberLxy == 0 ? 0 : gOffsetNumberLxy - 8;
        pstorage_load((uint8_t *)&g_StorageActive, &block_handle, 4, offsetNB);
        pstorage_load((uint8_t *)&g_AddressActive, &block_handle, 4, offsetNB + 4);
        g_StorageBegin = g_StorageActive;
        g_AddressBegin = g_AddressActive;
        g_bin_size = 0;
    }
}

/*********************************************************************
 * @fn      elara_flash_write
 *
 * @brief   Writes 'len' bytes to the internal flash.
 *
 * @param   len - len%4 must be zero and len must < PSTORAGE_FLASH_PAGE_SIZE
 * @param   pBuf - Valid buffer space at least as big as 'len' X 4.
 *
 * @return
 */
#if 0
int elara_flash_write(uint16_t len, uint8_t *pBuf)
{
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_leda_handle[g_StorageActive], 0, &block_handle);
    pstorage_store(&block_handle, pBuf, len, g_AddressActive);
    g_bin_size += len;
    g_AddressActive += len;
    if (g_AddressActive >= PSTORAGE_FLASH_PAGE_SIZE)
    {
        g_AddressActive = 0;
        g_StorageActive++;
    }

    return 0;
}
#else
int elara_flash_write(uint16_t len, uint8_t *pBuf)
{
    pstorage_handle_t block_handle;
    uint8_t *p;
    if (gUseRAMUpdateAVR)
    {
        Leda_EnqueueArray(pBuf, len);
        g_bin_size += len;
        //g_AddressActive += len;
        invert_LED(2);
    }
    else
    {
        if (Leda_EmptyQueue())
        {
            p = Leda_EnqueueArray(pBuf, len);
            pstorage_block_identifier_get(&g_storage_leda_handle[g_StorageActive], 0, &block_handle);
            pstorage_store(&block_handle, p, len, g_AddressActive);
            g_bin_size += len;
            g_AddressActive += len;
            if (g_AddressActive >= PSTORAGE_FLASH_PAGE_SIZE)
            {
                g_AddressActive = 0;
                g_StorageActive++;
            }
        }
        else
        {
            p = Leda_EnqueueArray(pBuf, len);
        }
    }
    return 0;
}
static void elara_flash_write_next(uint16_t len, uint8_t *pBuf)
{
    pstorage_handle_t block_handle;
    uint8_t *p;
    uint8_t length;
    if (false == Leda_DequeueArray(len))//说明没有数据了
    {
        gIfWriteFinish = true;
    }
    else //说明还有数据，需继续写flash
    {
        p = Leda_GetCurrent(&length);
        pstorage_block_identifier_get(&g_storage_leda_handle[g_StorageActive], 0, &block_handle);
        pstorage_store(&block_handle, p, length, g_AddressActive);
        g_bin_size += length;
        g_AddressActive += length;
        if (g_AddressActive >= PSTORAGE_FLASH_PAGE_SIZE)
        {
            g_AddressActive = 0;
            g_StorageActive++;
        }
    }
}
#endif

bool elara_flash_if_can_write_done(void)
{
    return gIfWriteFinish;
}

static void elara_flash_write_done_continue(void)
{
    pstorage_handle_t block_handle;
    gOffsetNumberLxy = 0;
    pstorage_block_identifier_get(&g_storage_handle_lxy, 0, &block_handle);
    pstorage_store(&block_handle, (uint8_t*)&g_StorageActive, 4, gOffsetNumberLxy);
    pstorage_store(&block_handle, (uint8_t*)&g_AddressActive, 4, gOffsetNumberLxy + 4);
    gOffsetNumberLxy = 8;
}

int elara_flash_write_done(void)
{
    if (gUseRAMUpdateAVR)
    {
        return 0;
    }
    // 128 bytes align.
    uint32_t realAddressActive = ((g_AddressActive + (uint32_t)128 - 1) / (uint32_t)128) * (uint32_t)128; //4
    if (realAddressActive >= PSTORAGE_FLASH_PAGE_SIZE)
    {
        g_AddressActive = 0;
        g_StorageActive++;
    }
    else
    {
        g_AddressActive = realAddressActive;
    }
    if ((8 + gOffsetNumberLxy) >= 1032) // 1024 + 8
    {
        pstorage_clear(&g_storage_handle_lxy, 1024);
        return 1;
    }
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_lxy, 0, &block_handle);
    pstorage_store(&block_handle, (uint8_t*)&g_StorageActive, 4, gOffsetNumberLxy);
    pstorage_store(&block_handle, (uint8_t*)&g_AddressActive, 4, gOffsetNumberLxy + 4);
    gOffsetNumberLxy += 8;
    
    return 0;
}

#ifdef UPDATE_AVR_TEST
uint8_t g_blink2_short_bin[1068] = {
0x0C, 0x94, 0x5C, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00,
0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00,
0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00,
0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00,
0x0C, 0x94, 0x88, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00,
0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00,
0x0C, 0x94, 0x6E, 0x00, 0x0C, 0x94, 0x6E, 0x00, 0x00, 0x00, 0x00, 0x08, 0x00, 0x02, 0x01, 0x00,
0x00, 0x03, 0x04, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x04, 0x08,
0x10, 0x20, 0x40, 0x80, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,
0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x04, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x03, 0x03,
0x03, 0x03, 0x03, 0x03, 0x00, 0x00, 0x00, 0x00, 0x25, 0x00, 0x28, 0x00, 0x2B, 0x00, 0x00, 0x00,
0x00, 0x00, 0x24, 0x00, 0x27, 0x00, 0x2A, 0x00, 0x11, 0x24, 0x1F, 0xBE, 0xCF, 0xEF, 0xD8, 0xE0,
0xDE, 0xBF, 0xCD, 0xBF, 0x21, 0xE0, 0xA0, 0xE0, 0xB1, 0xE0, 0x01, 0xC0, 0x1D, 0x92, 0xA9, 0x30,
0xB2, 0x07, 0xE1, 0xF7, 0x0E, 0x94, 0x03, 0x02, 0x0C, 0x94, 0x13, 0x02, 0x0C, 0x94, 0x00, 0x00,
0x61, 0xE0, 0x8D, 0xE0, 0x0C, 0x94, 0x93, 0x01, 0x61, 0xE0, 0x8D, 0xE0, 0x0E, 0x94, 0xCC, 0x01,
0x62, 0xE3, 0x70, 0xE0, 0x80, 0xE0, 0x90, 0xE0, 0x0E, 0x94, 0xF5, 0x00, 0x60, 0xE0, 0x8D, 0xE0,
0x0E, 0x94, 0xCC, 0x01, 0x68, 0xE5, 0x72, 0xE0, 0x80, 0xE0, 0x90, 0xE0, 0x0C, 0x94, 0xF5, 0x00,
0x1F, 0x92, 0x0F, 0x92, 0x0F, 0xB6, 0x0F, 0x92, 0x11, 0x24, 0x2F, 0x93, 0x3F, 0x93, 0x8F, 0x93,
0x9F, 0x93, 0xAF, 0x93, 0xBF, 0x93, 0x80, 0x91, 0x01, 0x01, 0x90, 0x91, 0x02, 0x01, 0xA0, 0x91,
0x03, 0x01, 0xB0, 0x91, 0x04, 0x01, 0x30, 0x91, 0x00, 0x01, 0x23, 0xE0, 0x23, 0x0F, 0x2D, 0x37,
0x20, 0xF4, 0x01, 0x96, 0xA1, 0x1D, 0xB1, 0x1D, 0x05, 0xC0, 0x26, 0xE8, 0x23, 0x0F, 0x02, 0x96,
0xA1, 0x1D, 0xB1, 0x1D, 0x20, 0x93, 0x00, 0x01, 0x80, 0x93, 0x01, 0x01, 0x90, 0x93, 0x02, 0x01,
0xA0, 0x93, 0x03, 0x01, 0xB0, 0x93, 0x04, 0x01, 0x80, 0x91, 0x05, 0x01, 0x90, 0x91, 0x06, 0x01,
0xA0, 0x91, 0x07, 0x01, 0xB0, 0x91, 0x08, 0x01, 0x01, 0x96, 0xA1, 0x1D, 0xB1, 0x1D, 0x80, 0x93,
0x05, 0x01, 0x90, 0x93, 0x06, 0x01, 0xA0, 0x93, 0x07, 0x01, 0xB0, 0x93, 0x08, 0x01, 0xBF, 0x91,
0xAF, 0x91, 0x9F, 0x91, 0x8F, 0x91, 0x3F, 0x91, 0x2F, 0x91, 0x0F, 0x90, 0x0F, 0xBE, 0x0F, 0x90,
0x1F, 0x90, 0x18, 0x95, 0x3F, 0xB7, 0xF8, 0x94, 0x80, 0x91, 0x05, 0x01, 0x90, 0x91, 0x06, 0x01,
0xA0, 0x91, 0x07, 0x01, 0xB0, 0x91, 0x08, 0x01, 0x26, 0xB5, 0xA8, 0x9B, 0x05, 0xC0, 0x2F, 0x3F,
0x19, 0xF0, 0x01, 0x96, 0xA1, 0x1D, 0xB1, 0x1D, 0x3F, 0xBF, 0x66, 0x27, 0x78, 0x2F, 0x89, 0x2F,
0x9A, 0x2F, 0x62, 0x0F, 0x71, 0x1D, 0x81, 0x1D, 0x91, 0x1D, 0x42, 0xE0, 0x66, 0x0F, 0x77, 0x1F,
0x88, 0x1F, 0x99, 0x1F, 0x4A, 0x95, 0xD1, 0xF7, 0x08, 0x95, 0x8F, 0x92, 0x9F, 0x92, 0xAF, 0x92,
0xBF, 0x92, 0xCF, 0x92, 0xDF, 0x92, 0xEF, 0x92, 0xFF, 0x92, 0x6B, 0x01, 0x7C, 0x01, 0x0E, 0x94,
0xD2, 0x00, 0x4B, 0x01, 0x5C, 0x01, 0xC1, 0x14, 0xD1, 0x04, 0xE1, 0x04, 0xF1, 0x04, 0xF1, 0xF0,
0x0E, 0x94, 0x12, 0x02, 0x0E, 0x94, 0xD2, 0x00, 0x68, 0x19, 0x79, 0x09, 0x8A, 0x09, 0x9B, 0x09,
0x68, 0x3E, 0x73, 0x40, 0x81, 0x05, 0x91, 0x05, 0x70, 0xF3, 0x21, 0xE0, 0xC2, 0x1A, 0xD1, 0x08,
0xE1, 0x08, 0xF1, 0x08, 0x88, 0xEE, 0x88, 0x0E, 0x83, 0xE0, 0x98, 0x1E, 0xA1, 0x1C, 0xB1, 0x1C,
0xC1, 0x14, 0xD1, 0x04, 0xE1, 0x04, 0xF1, 0x04, 0x29, 0xF7, 0xDD, 0xCF, 0xFF, 0x90, 0xEF, 0x90,
0xDF, 0x90, 0xCF, 0x90, 0xBF, 0x90, 0xAF, 0x90, 0x9F, 0x90, 0x8F, 0x90, 0x08, 0x95, 0x78, 0x94,
0x84, 0xB5, 0x82, 0x60, 0x84, 0xBD, 0x84, 0xB5, 0x81, 0x60, 0x84, 0xBD, 0x85, 0xB5, 0x82, 0x60,
0x85, 0xBD, 0x85, 0xB5, 0x81, 0x60, 0x85, 0xBD, 0xEE, 0xE6, 0xF0, 0xE0, 0x80, 0x81, 0x81, 0x60,
0x80, 0x83, 0xE1, 0xE8, 0xF0, 0xE0, 0x10, 0x82, 0x80, 0x81, 0x82, 0x60, 0x80, 0x83, 0x80, 0x81,
0x81, 0x60, 0x80, 0x83, 0xE0, 0xE8, 0xF0, 0xE0, 0x80, 0x81, 0x81, 0x60, 0x80, 0x83, 0xE1, 0xEB,
0xF0, 0xE0, 0x80, 0x81, 0x84, 0x60, 0x80, 0x83, 0xE0, 0xEB, 0xF0, 0xE0, 0x80, 0x81, 0x81, 0x60,
0x80, 0x83, 0xEA, 0xE7, 0xF0, 0xE0, 0x80, 0x81, 0x84, 0x60, 0x80, 0x83, 0x80, 0x81, 0x82, 0x60,
0x80, 0x83, 0x80, 0x81, 0x81, 0x60, 0x80, 0x83, 0x80, 0x81, 0x80, 0x68, 0x80, 0x83, 0x10, 0x92,
0xC1, 0x00, 0x08, 0x95, 0x83, 0x30, 0x81, 0xF0, 0x28, 0xF4, 0x81, 0x30, 0x99, 0xF0, 0x82, 0x30,
0xA1, 0xF0, 0x08, 0x95, 0x87, 0x30, 0xA9, 0xF0, 0x88, 0x30, 0xB9, 0xF0, 0x84, 0x30, 0xD1, 0xF4,
0x80, 0x91, 0x80, 0x00, 0x8F, 0x7D, 0x03, 0xC0, 0x80, 0x91, 0x80, 0x00, 0x8F, 0x77, 0x80, 0x93,
0x80, 0x00, 0x08, 0x95, 0x84, 0xB5, 0x8F, 0x77, 0x02, 0xC0, 0x84, 0xB5, 0x8F, 0x7D, 0x84, 0xBD,
0x08, 0x95, 0x80, 0x91, 0xB0, 0x00, 0x8F, 0x77, 0x03, 0xC0, 0x80, 0x91, 0xB0, 0x00, 0x8F, 0x7D,
0x80, 0x93, 0xB0, 0x00, 0x08, 0x95, 0xCF, 0x93, 0xDF, 0x93, 0x90, 0xE0, 0xFC, 0x01, 0xE4, 0x58,
0xFF, 0x4F, 0x24, 0x91, 0xFC, 0x01, 0xE0, 0x57, 0xFF, 0x4F, 0x84, 0x91, 0x88, 0x23, 0x49, 0xF1,
0x90, 0xE0, 0x88, 0x0F, 0x99, 0x1F, 0xFC, 0x01, 0xE2, 0x55, 0xFF, 0x4F, 0xA5, 0x91, 0xB4, 0x91,
0x8C, 0x55, 0x9F, 0x4F, 0xFC, 0x01, 0xC5, 0x91, 0xD4, 0x91, 0x9F, 0xB7, 0x61, 0x11, 0x08, 0xC0,
0xF8, 0x94, 0x8C, 0x91, 0x20, 0x95, 0x82, 0x23, 0x8C, 0x93, 0x88, 0x81, 0x82, 0x23, 0x0A, 0xC0,
0x62, 0x30, 0x51, 0xF4, 0xF8, 0x94, 0x8C, 0x91, 0x32, 0x2F, 0x30, 0x95, 0x83, 0x23, 0x8C, 0x93,
0x88, 0x81, 0x82, 0x2B, 0x88, 0x83, 0x04, 0xC0, 0xF8, 0x94, 0x8C, 0x91, 0x82, 0x2B, 0x8C, 0x93,
0x9F, 0xBF, 0xDF, 0x91, 0xCF, 0x91, 0x08, 0x95, 0x0F, 0x93, 0x1F, 0x93, 0xCF, 0x93, 0xDF, 0x93,
0x1F, 0x92, 0xCD, 0xB7, 0xDE, 0xB7, 0x28, 0x2F, 0x30, 0xE0, 0xF9, 0x01, 0xE8, 0x59, 0xFF, 0x4F,
0x84, 0x91, 0xF9, 0x01, 0xE4, 0x58, 0xFF, 0x4F, 0x14, 0x91, 0xF9, 0x01, 0xE0, 0x57, 0xFF, 0x4F,
0x04, 0x91, 0x00, 0x23, 0xC9, 0xF0, 0x88, 0x23, 0x21, 0xF0, 0x69, 0x83, 0x0E, 0x94, 0x6A, 0x01,
0x69, 0x81, 0xE0, 0x2F, 0xF0, 0xE0, 0xEE, 0x0F, 0xFF, 0x1F, 0xEC, 0x55, 0xFF, 0x4F, 0xA5, 0x91,
0xB4, 0x91, 0x9F, 0xB7, 0xF8, 0x94, 0x8C, 0x91, 0x61, 0x11, 0x03, 0xC0, 0x10, 0x95, 0x81, 0x23,
0x01, 0xC0, 0x81, 0x2B, 0x8C, 0x93, 0x9F, 0xBF, 0x0F, 0x90, 0xDF, 0x91, 0xCF, 0x91, 0x1F, 0x91,
0x0F, 0x91, 0x08, 0x95, 0x08, 0x95, 0x0E, 0x94, 0x2F, 0x01, 0x0E, 0x94, 0x02, 0x02, 0x0E, 0x94,
0x70, 0x00, 0xC0, 0xE0, 0xD0, 0xE0, 0x0E, 0x94, 0x74, 0x00, 0x20, 0x97, 0xE1, 0xF3, 0x0E, 0x94,
0x00, 0x00, 0xF9, 0xCF, 0x08, 0x95, 0xF8, 0x94, 0xFF, 0xCF, 
0x8F, 0x2A,
};
extern uint8_t g_blink_bin[284];
extern uint8_t g_blink1_bin[292];
int elara_flash_read_init(void)
{
    g_bin_size = 1068;
    g_AddressActive = 0;
    return 0;
}
uint32_t g_AddressActiveTemp = 0;
uint8_t gLedaFlag = 0;
void elara_flash_read( uint16_t len, uint8_t *pBuf )
{
    memcpy(pBuf, &g_blink2_short_bin[g_AddressActive], len);
    g_AddressActive += len;
    if (gLedaFlag == 1)
    {
        g_AddressActiveTemp = g_AddressActive;
    }
    gLedaFlag++;
}
#else
int elara_flash_read_init(void)
{
    if (gUseRAMUpdateAVR)
    {
        g_AddressActive = 0;
    }
    else
    {
        g_StorageActiveForRead = g_StorageBegin;
        g_AddressActiveForRead = g_AddressBegin;
    }
    return 0;
}

void elara_flash_read( uint16_t len, uint8_t *pBuf )
{
    pstorage_handle_t block_handle;
    if (gUseRAMUpdateAVR)
    {
        Leda_DequeueBuf(pBuf, len);
    }
    else
    {
        pstorage_block_identifier_get(&g_storage_leda_handle[g_StorageActiveForRead], 0, &block_handle);
        pstorage_load(pBuf, &block_handle, len, g_AddressActiveForRead);
        g_AddressActiveForRead += len;
        if (g_AddressActiveForRead >= PSTORAGE_FLASH_PAGE_SIZE)
        {
            g_AddressActiveForRead = 0;
            g_StorageActiveForRead++;
        }
    }
}
#endif
/*********************************************************************
 * @fn      elara_flash_if_flash_enough
 *
 * @brief   Before write bin to flash, we should check flash memory big enough.
            (Should run elara_flash_write_init function first.)
 *
 * @param   binSize - The size of the bin
 *
 * @return  The aviliabel flash size, 0 fail.
 */
uint32_t elara_flash_if_flash_enough(uint32_t binSize)
{
    uint32_t allFlashSize;
    uint32_t usedSize = g_StorageActive * 1024 + g_AddressActive;
    // The aviliable flash size now.
    //uint32_t flashsizeAV = LEDA_FLASH_SIZE - (g_StorageActive * 1024 + g_AddressActive);
    uint32_t flashsizeAV = LEDA_FLASH_SIZE - usedSize;

    // 4 bytes align
    uint32_t real = ((binSize + (uint32_t)LEDA_FLASH_WORD_SIZE - 1) / (uint32_t)LEDA_FLASH_WORD_SIZE) * (uint32_t)LEDA_FLASH_WORD_SIZE;//4
    if (LEDA_FLASH_SIZE < usedSize)
    {
        goto enoughExits;
    }
    if (real < flashsizeAV)
    {
        return flashsizeAV;
    }
enoughExits:
    //ELARA_LED2 = 1;
    //ELARA_LED1 = 1;
    allFlashSize = LEDA_FLASH_SIZE;
    if (real < allFlashSize)
    {
        return 0xFFFFFFFF;
    }
    return 0;
}

int elara_flash_erase(void)
{
    g_StorageBegin = 0;
    g_AddressBegin = 0;
    g_StorageActive = 0;
    g_AddressActive = 0;
    elara_flash_write_done();
    
    gStorageClear = 0;
    #if 0
    for (int i = 0; i < 32; i++)
    {
        pstorage_clear(&g_storage_leda_handle[i], 1024);
    }
    #else
    pstorage_clear(&g_storage_leda_handle[gStorageClear], 1024);
    #endif
    return 1;
}
static void elara_flash_erase_next(void)
{
    pstorage_clear(&g_storage_leda_handle[gStorageClear], 1024);
}

// When lost connection or upgrade fail, we should save current active page and offset.
void elara_flash_lost_connected(void)
{
    elara_flash_write_done();
}

static void tethys_pstorage_callback_handler1(pstorage_handle_t *p_handle,
        uint8_t             op_code,
        uint32_t            result,
        uint8_t            *p_data,
        uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        //tethys_handle_pstorage_store1(result);
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
            invert_LED(2);
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        elara_flash_write_next(data_len, p_data);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        invert_LED(1);
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
            gStorageClear++;
            if (gStorageClear >= 32)
            {
                close_LED(1);
                leda_tell_app_transfer_bin(LEDA_FLASH_SIZE);
                break;
            }
            app_timer_start(m_file_timer_id, APP_TIMER_TICKS(100, 0), NULL);
        }
        else//Try again.
        {
            LOG("Clear operation failed.\r\n");
            app_timer_start(m_file_timer_id, APP_TIMER_TICKS(100, 0), NULL);
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        //tethys_handle_pstorage_update1(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            //app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void pstorage_callback_handler(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Store operation successful.\r\n");
        }
        else
        {
            LOG("Store operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
            elara_flash_write_done_continue();
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:
        //handle_pstorage_update(result);
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            //app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void pstorage_callback_handler_lxy1(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:

        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            //app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static void pstorage_callback_handler_temp(pstorage_handle_t *p_handle,
                                      uint8_t             op_code,
                                      uint32_t            result,
                                      uint8_t            *p_data,
                                      uint32_t            data_len)
{
    LOG("p_handle=0x%x, data_len=%d\r\n", p_handle, data_len);
    switch(op_code)
    {
    case PSTORAGE_STORE_OP_CODE:
        
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_CLEAR_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Clear operation successful.\r\n");
        }
        else
        {
            LOG("Clear operation failed.\r\n");
        }
        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_UPDATE_OP_CODE:

        // Source memory can now be reused or freed.
        break;
    case PSTORAGE_LOAD_OP_CODE:
        if (result == NRF_SUCCESS)
        {
            LOG("Load operation successful.\r\n");
            //app_trace_dump(p_data, data_len);
        }
        else
        {
            LOG("Load operation failed.\r\n");
        }
        break;
    }
}

static int gErrorCode = 0;
void leda_save_error(int errcode)
{
    static uint16_t blocknum = BLOCK_NUM_ERROR;
    static uint16_t offset = 0;
    pstorage_handle_t block_handle;
    pstorage_block_identifier_get(&g_storage_handle_lxy1, blocknum, &block_handle);
    gErrorCode = errcode;
    pstorage_update(&block_handle, (uint8_t*)&gErrorCode, 4, offset);
    offset += 4;
    if (offset >= BLOCK_SIZE)
    {
        blocknum++;
        if (blocknum >= BLOCK_NUM_SYS_RUN_TIME)
        {
            blocknum = BLOCK_NUM_ERROR;
        }
        offset = 0;
    }
}

#ifdef LEDA_MONITOR_SYS_RUN_TIME
void leda_save_sys_run_time(void)
{
    static uint32_t sys_run_minutes = 0;
    static uint16_t blocknum = BLOCK_NUM_SYS_RUN_TIME;
    static uint16_t offset = 0;
    pstorage_handle_t block_handle;

    sys_run_minutes++;
    if (gLedaIsUpdattingAVR)
    {
        return;
    }
    pstorage_block_identifier_get(&g_storage_handle_lxy1, blocknum, &block_handle);
    pstorage_update(&block_handle, (uint8_t*)&sys_run_minutes, 4, offset);
    offset += 4;
    if (offset >= BLOCK_SIZE)
    {
        blocknum++;
        if (blocknum >= BLOCK_COUNT)
        {
            blocknum = BLOCK_NUM_SYS_RUN_TIME;
        }
        offset = 0;
    }
}
#endif

