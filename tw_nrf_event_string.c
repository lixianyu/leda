#include "ble.h"
#include "ble_gap.h"
#include "ble_l2cap.h"
#include "ble_gattc.h"
#include "nrf_soc.h"
#include "mimas_log.h"

/* len must big than 39 bytes! */
void nrf_events_strings_log(uint16_t evt_id)
{
    char *pStr = "BLE_EVT_INVALID";
    switch (evt_id)
    {
    case BLE_EVT_TX_COMPLETE:
        pStr = "BLE_EVT_TX_COMPLETE";
        break;
    case BLE_EVT_USER_MEM_REQUEST:
        pStr = "BLE_EVT_USER_MEM_REQUEST";
        break;
    case BLE_EVT_USER_MEM_RELEASE:
        pStr = "BLE_EVT_USER_MEM_RELEASE";
        break;
    //////////////////////////////
    case BLE_GAP_EVT_CONNECTED:
        pStr = "BLE_GAP_EVT_CONNECTED";
        break;
    case BLE_GAP_EVT_DISCONNECTED:
        pStr = "BLE_GAP_EVT_DISCONNECTED";
        break;
    case BLE_GAP_EVT_CONN_PARAM_UPDATE:
        pStr = "BLE_GAP_EVT_CONN_PARAM_UPDATE";
        break;
    case BLE_GAP_EVT_SEC_PARAMS_REQUEST:
        pStr = "BLE_GAP_EVT_SEC_PARAMS_REQUEST";
        break;
    case BLE_GAP_EVT_SEC_INFO_REQUEST:
        pStr = "BLE_GAP_EVT_SEC_INFO_REQUEST";
        break;
    case BLE_GAP_EVT_PASSKEY_DISPLAY:
        pStr = "BLE_GAP_EVT_PASSKEY_DISPLAY";
        break;
    case BLE_GAP_EVT_AUTH_KEY_REQUEST:                 /**< Request to provide an authentication key. @ref ble_gap_evt_auth_key_request_t */
        pStr = "BLE_GAP_EVT_AUTH_KEY_REQUEST";
        break;
    case BLE_GAP_EVT_AUTH_STATUS:                      /**< Authentication procedure completed with status. @ref ble_gap_evt_auth_status_t */
        pStr = "BLE_GAP_EVT_AUTH_STATUS";
        break;
    case BLE_GAP_EVT_CONN_SEC_UPDATE:                  /**< Connection security updated. @ref ble_gap_evt_conn_sec_update_t */
        pStr = "BLE_GAP_EVT_CONN_SEC_UPDATE";
        break;
    case BLE_GAP_EVT_TIMEOUT:                          /**< Timeout expired. @ref ble_gap_evt_timeout_t */
        pStr = "BLE_GAP_EVT_TIMEOUT";
        break;
    case BLE_GAP_EVT_RSSI_CHANGED:                     /**< RSSI report. @ref ble_gap_evt_rssi_changed_t */
        pStr = "BLE_GAP_EVT_RSSI_CHANGED";
        break;
    case BLE_GAP_EVT_ADV_REPORT:                       /**< Advertising report. @ref ble_gap_evt_adv_report_t */
        pStr = "BLE_GAP_EVT_ADV_REPORT";
        break;
    case BLE_GAP_EVT_SEC_REQUEST:                      /**< Security Request. @ref ble_gap_evt_sec_request_t */
        pStr = "BLE_GAP_EVT_SEC_REQUEST";
        break;
    case BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST:        /**< Connection Parameter Update Request. @ref ble_gap_evt_conn_param_update_request_t */
        pStr = "BLE_GAP_EVT_CONN_PARAM_UPDATE_REQUEST";
        break;
    case BLE_GAP_EVT_SCAN_REQ_REPORT:
        pStr = "BLE_GAP_EVT_SCAN_REQ_REPORT";
        break;
    //////////////////////////////
    case BLE_GATTC_EVT_PRIM_SRVC_DISC_RSP:  /**< Primary Service Discovery Response event. @ref ble_gattc_evt_prim_srvc_disc_rsp_t */
        pStr = "BLE_GATTC_EVT_PRIM_SRVC_DISC_RSP";
        break;
    case BLE_GATTC_EVT_REL_DISC_RSP:                             /**< Relationship Discovery Response event. @ref ble_gattc_evt_rel_disc_rsp_t */
        pStr = "BLE_GATTC_EVT_REL_DISC_RSP";
        break;
    case BLE_GATTC_EVT_CHAR_DISC_RSP:                            /**< Characteristic Discovery Response event. @ref ble_gattc_evt_char_disc_rsp_t */
        pStr = "BLE_GATTC_EVT_CHAR_DISC_RSP";
        break;
    case BLE_GATTC_EVT_DESC_DISC_RSP:                            /**< Descriptor Discovery Response event. @ref ble_gattc_evt_desc_disc_rsp_t */
        pStr = "BLE_GATTC_EVT_DESC_DISC_RSP";
        break;
    case BLE_GATTC_EVT_CHAR_VAL_BY_UUID_READ_RSP:                /**< Read By UUID Response event. @ref ble_gattc_evt_char_val_by_uuid_read_rsp_t */
        pStr = "BLE_GATTC_EVT_CHAR_VAL_BY_UUID_READ_RSP";
        break;
    case BLE_GATTC_EVT_READ_RSP:                                 /**< Read Response event. @ref ble_gattc_evt_read_rsp_t */
        pStr = "BLE_GATTC_EVT_READ_RSP";
        break;
    case BLE_GATTC_EVT_CHAR_VALS_READ_RSP:                       /**< Read multiple Response event. @ref ble_gattc_evt_char_vals_read_rsp_t */
        pStr = "BLE_GATTC_EVT_CHAR_VALS_READ_RSP";
        break;
    case BLE_GATTC_EVT_WRITE_RSP:                                /**< Write Response event. @ref ble_gattc_evt_write_rsp_t */
        pStr = "BLE_GATTC_EVT_WRITE_RSP";
        break;
    case BLE_GATTC_EVT_HVX:                                      /**< Handle Value Notification or Indication event. @ref ble_gattc_evt_hvx_t */
        pStr = "BLE_GATTC_EVT_HVX";
        break;
    case BLE_GATTC_EVT_TIMEOUT:                                   /**< Timeout event. @ref ble_gattc_evt_timeout_t */
        pStr = "BLE_GATTC_EVT_TIMEOUT";
        break;
    //////////////////////////////
    case BLE_GATTS_EVT_WRITE:       /**< Write operation performed. @ref ble_gatts_evt_write_t */
        pStr = "BLE_GATTS_EVT_WRITE";
        break;
    case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:             /**< Read/Write Authorization request.@ref ble_gatts_evt_rw_authorize_request_t */
        pStr = "BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST";
        break;
    case BLE_GATTS_EVT_SYS_ATTR_MISSING:                 /**< A persistent system attribute access is pending, awaiting a sd_ble_gatts_sys_attr_set(). @ref ble_gatts_evt_sys_attr_missing_t */
        pStr = "BLE_GATTS_EVT_SYS_ATTR_MISSING";
        break;
    case BLE_GATTS_EVT_HVC:                              /**< Handle Value Confirmation. @ref ble_gatts_evt_hvc_t */
        pStr = "BLE_GATTS_EVT_HVC";
        break;
    case BLE_GATTS_EVT_SC_CONFIRM:                       /**< Service Changed Confirmation. No additional event structure applies. */
        pStr = "BLE_GATTS_EVT_SC_CONFIRM";
        break;
    case BLE_GATTS_EVT_TIMEOUT:                           /**< Timeout. @ref ble_gatts_evt_timeout_t */
        pStr = "BLE_GATTS_EVT_TIMEOUT";
        break;
    //////////////////////////////
    case BLE_L2CAP_EVT_RX: /**< L2CAP packet received. */
        pStr = "BLE_L2CAP_EVT_RX";
        break;
    }
    LOG(LEVEL_INFO, "evt_id = %s", pStr);
}

void nrf_sys_events_strings_log(uint32_t evt_id)
{
    char *pStr = "BLE_SYS_EVT_INVALID";
    switch (evt_id)
    {
    case NRF_EVT_HFCLKSTARTED:
        pStr = "NRF_EVT_HFCLKSTARTED";
        break;
    case NRF_EVT_POWER_FAILURE_WARNING:
        pStr = "NRF_EVT_POWER_FAILURE_WARNING";
        break;
    case NRF_EVT_FLASH_OPERATION_SUCCESS:
        pStr = "NRF_EVT_FLASH_OPERATION_SUCCESS";
        break;
    case NRF_EVT_FLASH_OPERATION_ERROR:
        pStr = "NRF_EVT_FLASH_OPERATION_ERROR";
        break;
    case NRF_EVT_RADIO_BLOCKED:
        pStr = "NRF_EVT_RADIO_BLOCKED";
        break;
    case NRF_EVT_RADIO_CANCELED:
        pStr = "NRF_EVT_RADIO_CANCELED";
        break;
    case NRF_EVT_RADIO_SIGNAL_CALLBACK_INVALID_RETURN:
        pStr = "NRF_EVT_RADIO_SIGNAL_CALLBACK_INVALID_RETURN";
        break;
    case NRF_EVT_RADIO_SESSION_IDLE:
        pStr = "NRF_EVT_RADIO_SESSION_IDLE";
        break;
    case NRF_EVT_RADIO_SESSION_CLOSED:
        pStr = "NRF_EVT_RADIO_SESSION_CLOSED";
        break;
    case NRF_EVT_NUMBER_OF_EVTS:
        pStr = "NRF_EVT_NUMBER_OF_EVTS";
        break;
    }
    //LOG(LEVEL_INFO, "sys evt_id = %s", pStr);
    printf("%s\r\n", pStr);
}
