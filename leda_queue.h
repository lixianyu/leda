
#ifndef __LEDA_QUEUE_H__
#define __LEDA_QUEUE_H__

#include <stdbool.h>
#include <stdint.h>

#define LEDA_QUEUE_SIZE    15360 //15K

typedef struct queue   
{  
    uint8_t *pBase;  
    uint32_t front;//指向队列第一个元素
    uint32_t rear;//指向队列最后一个元素的下一个元素
    uint32_t maxsize;//循环队列的最大存储空间
    int32_t count;
}QUEUE,*PQUEUE;  

extern void Leda_InitQueue(void);
extern uint8_t * Leda_GetCurrent(uint8_t *pLen);
extern bool Leda_EmptyQueue(void);
extern uint8_t* Leda_EnqueueArray(uint8_t *val, uint8_t len);
extern bool Leda_DequeueArray(uint8_t len);
extern void Leda_DequeueBuf(uint8_t *val, uint8_t len);
#endif //__LEDA_QUEUE_H__
