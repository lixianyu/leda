#ifndef CUSTOM_BOARD_H
#define CUSTOM_BOARD_H

#define LEDA_HARDWARE_REVISION 2

#ifdef AKII
// LEDs definitions for CUSTOM_BOARD AK-II
#define LEDS_NUMBER    5

#define LED_START      18
#define LED_1          18
#define LED_2          19
#define LED_3          20
#define LED_4          21
#define LED_5          22
#define LED_STOP       22

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4, LED_5 }

#define BSP_LED_0      LED_1
#define BSP_LED_1      LED_2
#define BSP_LED_2      LED_3
#define BSP_LED_3      LED_4
#define BSP_LED_4      LED_5

#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)
#define BSP_LED_2_MASK (1<<BSP_LED_2)
#define BSP_LED_3_MASK (1<<BSP_LED_3)
#define BSP_LED_4_MASK (1<<BSP_LED_4)

#define LEDS_MASK      (BSP_LED_0_MASK | BSP_LED_1_MASK | BSP_LED_2_MASK | BSP_LED_3_MASK | BSP_LED_4_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK  (~(LEDS_MASK))
////////////////////////////////////////////////////////////////////////////////////////////////////

#define BUTTONS_NUMBER 2

#define BUTTON_START   16
#define BUTTON_1       16
#define BUTTON_2       17
#define BUTTON_STOP    17
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1, BUTTON_2 }

#define BSP_BUTTON_0   BUTTON_1
#define BSP_BUTTON_1   BUTTON_2

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)
#define BSP_BUTTON_1_MASK (1<<BSP_BUTTON_1)

#define BUTTONS_MASK   0x00030000
#if 0
#define RX_PIN_NUMBER  14
#define TX_PIN_NUMBER  12
#define CTS_PIN_NUMBER 10
#define RTS_PIN_NUMBER 8
#define HWFC           false
#else
#define RX_PIN_NUMBER  9
#define TX_PIN_NUMBER  8
#define CTS_PIN_NUMBER 0xFF
#define RTS_PIN_NUMBER 0xFF
#define HWFC           false
#endif
#define LEDA_RX_PIN    9
#define LEDA_TX_PIN    8
#define AVR_RESET_PIN  10
#else // Not defined AKII
///////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////

#define BUTTONS_NUMBER 0
#define BUTTON_START   23
#define BUTTON_1       23
#define BUTTON_STOP    23
#define BUTTON_PULL    NRF_GPIO_PIN_PULLUP

#define BUTTONS_LIST { BUTTON_1 }

#define BSP_BUTTON_0   BUTTON_1

#define BSP_BUTTON_0_MASK (1<<BSP_BUTTON_0)

#define BUTTONS_MASK   BSP_BUTTON_0_MASK

////////////////////////////////////////////////////////////////////////////////////////////////////
/*
 On AKII board,pin 26,27 used for 32768; pin 16~17 for button and 18~22 used for led.
 pin 1,2,3 used for MPU6050.
 */

#define ADC_VBAT_PIN 1 //Should be 'NRF_ADC_CONFIG_INPUT_2'

/* LEDs */
#define LEDS_NUMBER    4

#define LED_START      12
#define LED_1          12
#define LED_2          13
#define LED_3          14
#define LED_4          15
#define LED_STOP       15

#define LEDS_LIST { LED_1, LED_2, LED_3, LED_4}

#define BSP_LED_0      LED_4 //green
#define BSP_LED_1      LED_3 //red
#define BSP_LED_2      LED_2 //red
#define BSP_LED_3      LED_1 //green

#define BSP_LED_0_MASK (1<<BSP_LED_0)
#define BSP_LED_1_MASK (1<<BSP_LED_1)
#define BSP_LED_2_MASK (1<<BSP_LED_2)
#define BSP_LED_3_MASK (1<<BSP_LED_3)

#define LEDS_MASK (BSP_LED_0_MASK|BSP_LED_1_MASK|BSP_LED_2_MASK|BSP_LED_3_MASK)
/* all LEDs are lit when GPIO is low */
#define LEDS_INV_MASK (~(LEDS_MASK))

#if (LEDA_HARDWARE_REVISION == 2)
#define LEDA_RX_PIN    10
#define LEDA_TX_PIN    9
#define AVR_RESET_PIN  8
#define IO_VBUS_PIN    4
#define IO_CHRG_PIN    1
#define IO_VBAT_PIN    2
#define IO_VDD_PIN     6
#define IO_SWTTL_PIN   30
#else
#define LEDA_RX_PIN    9
#define LEDA_TX_PIN    8
#define AVR_RESET_PIN  10
#define IO_VBUS_PIN    2
#define IO_CHRG_PIN    3
#define IO_VBAT_PIN    4
#define IO_VDD_PIN     5
#ifdef LEDA_LOWBAT_IO
#define IO_LOWBAT_PIN  1
#endif
#endif
#endif // AKII

#define IO_PULLUP_PIN 29

#endif // CUSTOM_BOARD_H
