
/*
 * Auto generated Run-Time-Environment Component Configuration File
 *      *** Do not modify ! ***
 *
 * Project: 'ble_app_proximity_s110_pca10028' 
 * Target:  'nrf51822_xxaa_s110' 
 */

#ifndef RTE_COMPONENTS_H
#define RTE_COMPONENTS_H

#define GPIOTE_CONFIG_NUM_OF_LOW_POWER_EVENTS
#define GPIOTE_ENABLED
  #define GPIOTE_CONFIG_NUM_OF_LOW_POWER_EVENTS
#define RNG_ENABLED
#define S110
#define SOFTDEVICE_PRESENT
#define SWI_DISABLE0
#define SWI_DISABLE1
#define WDT_ENABLED

#endif /* RTE_COMPONENTS_H */
