/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "tethys_battery.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "nrf_adc.h"
#include "app_uart.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "mimas_run_in_ram.h"
#include "mimas_bsp.h"

#define BATTERY_YI_FEN 5 // (BATTERY_FULL - BATTERY_LOW) / 100

APP_TIMER_DEF(m_tethys_battery_timer_id);
static void tethys_battery_timeout_handler(void *p_context);
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
extern bool leda_if_connected(void);
extern uint16_t g_battery_voltage_mv;
extern bool gLedaIsUpdattingAVR;
extern bool gIfUSBIn;
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void init_battery(void)
{
    app_timer_create(&m_tethys_battery_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     tethys_battery_timeout_handler);
    //app_timer_start(m_tethys_battery_timer_id, APP_TIMER_TICKS(2000, 0), NULL);
}

#if 0
__STATIC_INLINE uint32_t nrf_gpio_pin_state(uint32_t pin_number)
{
    uint32_t pin_state = ((NRF_GPIO->OUT >> pin_number) & 1UL);
    return pin_state;
}
#endif

void tethys_battery_timeout_handler(void *p_context)
{
    if (gIfUSBIn)
    {
        return;
    }
    if (leda_if_connected()) //如果连接态
    {
        if (!gLedaIsUpdattingAVR)
        {
            nrf_adc_start();
        }
        app_timer_start(m_tethys_battery_timer_id, APP_TIMER_TICKS(15000, 0), NULL);
    }
    else
    {
        nrf_adc_start();
        app_timer_start(m_tethys_battery_timer_id, APP_TIMER_TICKS(5000, 0), NULL);
    }
}

void tethys_stop_battery_check(void)
{
    app_timer_stop(m_tethys_battery_timer_id);
}

void tethys_battery_calculate(void)
{//刚关闭锁时，电压低，所以得等一会儿
    app_timer_stop(m_tethys_battery_timer_id);
    app_timer_start(m_tethys_battery_timer_id, APP_TIMER_TICKS(5000, 0), NULL);
}

bool tethys_if_low_battery(void)
{
    if (g_battery_voltage_mv < BATTERY_LOW)
    {
        return true;
    }
    return false;
}

#ifdef TETHYS_ADC
// mvolts should less than 4200 mv
__INLINE uint8_t batteryLevelInPercent(const uint16_t mvolts)
{
    uint8_t temp;
    uint8_t battery_level;

    if (mvolts >= 4180)
    {
        battery_level = 100;
    }
    else if (mvolts > 4150)
    {
        temp  = (mvolts - 4150) / BATTERY_YI_FEN;
        battery_level = 90 + temp;
    }
    else if (mvolts > 4100)
    {
        temp  = (mvolts - 4100) / BATTERY_YI_FEN;
        battery_level = 80 + temp;
    }
    else if (mvolts > 4050)
    {
        temp  = (mvolts - 4050) / BATTERY_YI_FEN;
        battery_level = 70 + temp;
    }
    else if (mvolts > 4000)
    {
        temp  = (mvolts - 4000) / BATTERY_YI_FEN;
        battery_level = 60 + temp;
    }
    else if (mvolts > 3950)
    {
        temp  = (mvolts - 3950) / BATTERY_YI_FEN;
        battery_level = 50 + temp;
    }
    else if (mvolts > 3900)
    {
        temp  = (mvolts - 3900) / BATTERY_YI_FEN;
        battery_level = 40 + temp;
    }
    else if (mvolts > 3850)
    {
        temp  = (mvolts - 3850) / BATTERY_YI_FEN;
        battery_level = 30 + temp;
    }
    else if (mvolts > 3800)
    {
        temp  = (mvolts - 3800) / BATTERY_YI_FEN;
        battery_level = 20 + temp;
    }
    else if (mvolts >= 3750)
    {
        temp  = (mvolts - 3750) / BATTERY_YI_FEN;
        battery_level = 10 + temp;
    }
    else
    {
        if (mvolts > 3740)
        {
            battery_level = 10;
        }
        else if (mvolts > 3730)
        {
            battery_level = 8;
        }
        else if (mvolts > 3720)
        {
            battery_level = 6;
        }
        else if (mvolts > 3710)
        {
            battery_level = 4;
        }
        else if (mvolts > BATTERY_LOW)
        {
            battery_level = 2;
        }
        else
        {
            battery_level = 0;
        }
    }

    return battery_level;
}
#else
static __INLINE uint8_t batteryLevelInPercent(const uint16_t mvolts)
{
    uint8_t temp;
    uint8_t battery_level;

    if (mvolts >= 3000)
    {
        battery_level = 100;
    }
    else if (mvolts > 2900)
    {
        temp  = (mvolts - 2900) / 10;
        battery_level = 90 + temp;
    }
    else if (mvolts > 2800)
    {
        temp  = (mvolts - 2800) / 10;
        battery_level = 80 + temp;
    }
    else if (mvolts > 2700)
    {
        temp  = (mvolts - 2700) / 10;
        battery_level = 70 + temp;
    }
    else if (mvolts > 2600)
    {
        temp  = (mvolts - 2600) / 10;
        battery_level = 60 + temp;
    }
    else if (mvolts > 2500)
    {
        temp  = (mvolts - 2500) / 10;
        battery_level = 50 + temp;
    }
    else if (mvolts > 2400)
    {
        temp  = (mvolts - 2400) / 10;
        battery_level = 40 + temp;
    }
    else if (mvolts > 2300)
    {
        temp  = (mvolts - 2300) / 10;
        battery_level = 30 + temp;
    }
    else if (mvolts > 2200)
    {
        temp  = (mvolts - 2200) / 10;
        battery_level = 20 + temp;
    }
    else if (mvolts >= 2100)
    {
        temp  = (mvolts - 2100) / 10;
        battery_level = 10 + temp;
    }
    else
    {
        if (mvolts > 2000)
        {
            battery_level = 8;
        }
        else if (mvolts > 1900)
        {
            battery_level = 5;
        }
        else if (mvolts > 1800)
        {
            battery_level = 1;
        }
        else
        {
            battery_level = 0;
        }
    }

    return battery_level;
}
#endif

