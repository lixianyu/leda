#ifndef ELARA_FILE_ERROR_H
#define ELARA_FILE_ERROR_H

#ifdef __cplusplus
extern "C"
{
#endif

/*********************************************************************
 * INCLUDES
 */


/*********************************************************************
 * CONSTANTS
 */

/*********************************************************************
 * MACROS
 */
#define STK_ERROR_CMD_1    -1
#define STK_ERROR_CMD_2    -2
#define STK_ERROR_CMD_3    -3
#define STK_ERROR_CMD_4    -4
#define STK_ERROR_CMD_5    -5

#define STK_ERROR_CMD_6    -6
#define STK_ERROR_CMD_7    -7
#define STK_ERROR_CMD_8    -8

#define STK_ERROR_CMD_9    -9
#define STK_ERROR_CMD_10    -10
#define STK_ERROR_CMD_11    -11
#define STK_ERROR_CMD_12    -12
#define STK_ERROR_CMD_13    -13
#define STK_ERROR_CMD_14    -14
#define STK_ERROR_CMD_15    -15

#define STK_ERROR_CMD_16    -16
#define STK_ERROR_CMD_17    -17
#define STK_ERROR_CMD_18    -18
#define STK_ERROR_CMD_19    -19
#define STK_ERROR_CMD_20    -20
#define STK_ERROR_CMD_21    -21
#define STK_ERROR_CMD_22    -22
#define STK_ERROR_CMD_23    -23

#define STK_ERROR_CMD_24    -24
#define STK_ERROR_CMD_25    -25
#define STK_ERROR_CMD_26    -26
#define STK_ERROR_CMD_27    -27
#define STK_ERROR_CMD_28    -28
#define STK_ERROR_CMD_29    -29
#define STK_ERROR_CMD_30    -30
#define STK_ERROR_CMD_31    -31
#define STK_ERROR_CMD_32    -32
#define STK_ERROR_CMD_33    -33
#define STK_ERROR_CMD_34    -34
#define STK_ERROR_CMD_35    -35
#define STK_ERROR_CMD_36    -36

#define STK_ERROR_CMD_37    -37
#define STK_ERROR_CMD_38    -38
#define STK_ERROR_CMD_39    -39
#define STK_ERROR_CMD_40    -40
#define STK_ERROR_CMD_41    -41
#define STK_ERROR_CMD_42    -42
#define STK_ERROR_CMD_43    -43

#define STK_ERROR_CMD_44    -44
#define STK_ERROR_CMD_45    -45
#define STK_ERROR_CMD_46    -46
#define STK_ERROR_CMD_47    -47
#define STK_ERROR_CMD_48    -48
#define STK_ERROR_CMD_49    -49
#define STK_ERROR_CMD_50    -50
#define STK_ERROR_CMD_51    -51
#define STK_ERROR_CMD_52    -52

#define STK_ERROR_CMD_53    -53
#define STK_ERROR_CMD_54    -54
#define STK_ERROR_CMD_55    -55
#define STK_ERROR_CMD_56    -56
#define STK_ERROR_CMD_57    -57
#define STK_ERROR_CMD_58    -58
#define STK_ERROR_CMD_59    -59
#define STK_ERROR_CMD_60    -60

#define STK_ERROR_CMD_61    -61
#define STK_ERROR_CMD_62    -62
#define STK_ERROR_CMD_63    -63
#define STK_ERROR_CMD_64    -64
#define STK_ERROR_CMD_65    -65
#define STK_ERROR_CMD_66    -66
#define STK_ERROR_CMD_67    -67
#define STK_ERROR_CMD_68    -68
#define STK_ERROR_CMD_69    -69

#define STK_ERROR_CMD_70   -70
#define STK_ERROR_CMD_71   -71
#define STK_ERROR_CMD_72   -72
#define STK_ERROR_CMD_73   -73
#define STK_ERROR_CMD_74   -74

#define STK_ERROR_CMD_75   -75
#define STK_ERROR_CMD_76   -76

#define STK_ERROR_CMD_77   -77
#define STK_ERROR_CMD_78   -78
#define STK_ERROR_CMD_79   -79
#define STK_ERROR_CMD_80   -80
#define STK_ERROR_CMD_81   -81
#define STK_ERROR_CMD_82   -82
#define STK_ERROR_CMD_83   -83

#define STK_ERROR_CMD_84   -84
#define STK_ERROR_CMD_85   -85
#define STK_ERROR_CMD_86   -86

#define STK_ERROR_CMD_87   -87
#define STK_ERROR_CMD_88   -88
#define STK_ERROR_CMD_89   -89
#define STK_ERROR_CMD_90   -90
#define STK_ERROR_CMD_91   -91
#define STK_ERROR_CMD_92   -92

#define STK_ERROR_CMD_93   -93
#define STK_ERROR_CMD_94   -94
#define STK_ERROR_CMD_95   -95
#define STK_ERROR_CMD_96   -96
#define STK_ERROR_CMD_97   -97
#define STK_ERROR_CMD_98   -98

#define STK_ERROR_CMD_99   -99
#define STK_ERROR_CMD_100   -100
#define STK_ERROR_CMD_101   -101
#define STK_ERROR_CMD_102   -102
#define STK_ERROR_CMD_103   -103

#define ELARA_ERROR_104   -104
#define ELARA_ERROR_105   -105
/*********************************************************************
 * TYPEDEFS
 */

/*********************************************************************
 * GLOBAL VARIABLES
 */

/*********************************************************************
 * FUNCTIONS
 */


/*********************************************************************
*********************************************************************/

#ifdef __cplusplus
}
#endif

#endif
