/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include <math.h>
#include "tethys_led.h"
#include "boards.h"
#include "nrf_gpio.h"
#include "nrf_delay.h"
#include "nrf_drv_gpiote.h"
#include "app_uart.h"
#include "app_error.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "app_pwm.h"
#include "led_softblink.h"
#include "mimas_run_in_ram.h"
#include "mimas_bsp.h"

typedef enum
{
    LED_DH_NOTHING,
    LED_DH_kaiji,
    LED_DH_Softblink_kaiji,
    LED_DH_alert,
    LED_DH_blink,
    LED_DH_blink_error,
    LED_DH_connected,
    LED_DH_BREATH,
} LED_DH_t;

APP_TIMER_DEF(m_led_timer_id);
APP_TIMER_DEF(m_softled_timer_id);
static void led_timeout_handler(void *p_context);
static void softled_timeout_handler(void *p_context);
static void resolve_blink(void);
static void resolve_blink_alert(void);
static void resolve_blink_kaiji(void);
static void resolve_blink_connected(void);
static void resolve_Softblink_kaiji(void);
static void leda_led_softblink_init(uint32_t mask);
static void resolve_blink_error(void);
#ifdef TETHYS_PWM
static void resolve_pwm_1(void);
#endif
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
static LED_DH_t mTethysLEDstate = LED_DH_NOTHING;
static uint8_t mTethysDH = 0;
static uint8_t mTethysLEDcount = 0;
static uint32_t gLedNum;//Or mask.
static bool mStop = false;
//static LED_DH_callback gLedCallback = NULL;
#ifdef TETHYS_PWM
static bool gPWMstop = false;
static uint32_t millis = 5;
#endif
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void init_LED(bool flag)
{
    app_timer_create(&m_led_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     led_timeout_handler);
    app_timer_create(&m_softled_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     softled_timeout_handler);
    if (flag)
    {
        led_blink_kaiji();
        //led_Softblink_start();
    }
}

//At the same time , we close the other leds.
void open_LED(uint8_t led)
{
    switch (led)
    {
        case 1:
            LEDS_ON(BSP_LED_0_MASK);
            break;
        case 2:
            LEDS_ON(BSP_LED_1_MASK);
            break;
        case 3:
            LEDS_ON(BSP_LED_2_MASK);
            break;
        case 4:
            LEDS_ON(BSP_LED_3_MASK);
            break;
#ifdef AKII
        case 5:
            LEDS_ON(BSP_LED_4_MASK);
            break;
#else
        case 5:
            LEDS_ON(BSP_LED_0_MASK);
            break;
#endif
        default:
            //shit here...
            break;
    }
}

void close_LED(uint8_t led)
{
    switch (led)
    {
        case 1:
            LEDS_OFF(BSP_LED_0_MASK);
            break;
        case 2:
            LEDS_OFF(BSP_LED_1_MASK);
            break;
        case 3:
            LEDS_OFF(BSP_LED_2_MASK);
            break;
        case 4:
            LEDS_OFF(BSP_LED_3_MASK);
            break;
#ifdef AKII
        case 5:
            LEDS_OFF(BSP_LED_4_MASK);
            break;
#else
        case 5:
            LEDS_OFF(BSP_LED_0_MASK);
            break;
#endif
        default:
            //shit here...
            break;
    }
}

void invert_LED(uint8_t led)
{
    switch (led)
    {
        case 1:
            LEDS_INVERT(BSP_LED_0_MASK);
            break;
        case 2:
            LEDS_INVERT(BSP_LED_1_MASK);
            break;
        case 3:
            LEDS_INVERT(BSP_LED_2_MASK);
            break;
        case 4:
            LEDS_INVERT(BSP_LED_3_MASK);
            break;
#ifdef AKII
        case 5:
            LEDS_INVERT(BSP_LED_4_MASK);
            break;
#else
        case 5:
            LEDS_INVERT(BSP_LED_0_MASK);
            break;
#endif
        default:
            //shit here...
            break;
    }
}

void open_LEDs(void)
{
    LEDS_ON(LEDS_MASK);
}

void close_LEDS(void)
{
    LEDS_OFF(LEDS_MASK);
}

static void tethys_led_indication(LED_DH_t state)
{
    //uint32_t next_delay = 0;
    switch (state)
    {
        case LED_DH_kaiji:
            resolve_blink_kaiji();
            break;            
        case LED_DH_Softblink_kaiji:
            resolve_Softblink_kaiji();
            break;
        case LED_DH_blink:
            resolve_blink();
            break;
        case LED_DH_blink_error:
            resolve_blink_error();
            break;
        case LED_DH_alert:
            resolve_blink_alert();
            break;
        case LED_DH_connected:
            resolve_blink_connected();
            break;
#ifdef TETHYS_PWM
        case LED_DH_BREATH:
            if (!gPWMstop)
            {
                resolve_pwm_1();
            }
            break;
#endif
        default:
            break;
    }
}

static void led_timeout_handler(void *p_context)
{
    tethys_led_indication(mTethysLEDstate);
}

static void softled_timeout_handler(void *p_context)
{
    led_Softblink_stop();
}
/////////////////////////////////////////////////////////////////////////////////////
// Blink one LED to indicate Error.
void led_blink_error(uint32_t ledmask)
{
    //close_LEDS();
    gLedNum = ledmask;
    mTethysLEDstate = LED_DH_blink_error;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(150, 0), NULL);
}

static void resolve_blink_error(void)
{
    #define DELAY_TIME_OFF 900
    #define DELAY_TIME_ON 100

    switch (mTethysDH)
    {
        case 0:
            LEDS_ON(gLedNum);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            LEDS_OFF(gLedNum);
            mTethysDH = 0;
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}

/////////////////////////////////////////////////////////////////////////////////////
// Blink one LED.
void led_blink(uint8_t lednumber)
{
    //close_LEDS();
    gLedNum = lednumber;
    mTethysLEDstate = LED_DH_blink;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    mStop = false;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(50, 0), NULL);
}

static void resolve_blink(void)
{
    #define DELAY_TIME_OFF 2000
    #define DELAY_TIME_ON 100

    switch (mTethysDH)
    {
        case 0:
            open_LED(gLedNum);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            close_LED(gLedNum);
            mTethysDH = 0;
            if (mStop == true)
            {
                open_LED(gLedNum);
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}

void led_blink_stop(void)
{
    //app_timer_stop(m_led_timer_id);
    mStop = true;
}
/////////////////////////////////////////////////////////////////////////////////////
// Blink one hang LED.There 4 hang
void led_blink_alert(uint16_t hang)
{
    gLedNum = hang;// Delay off time in ms.
    mTethysLEDstate = LED_DH_alert;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(50, 0), NULL);
}

static void resolve_blink_alert(void)
{
    #define DELAY_TIME_OFF 500
    #define DELAY_TIME_ON 100

    switch (mTethysDH)
    {
        case 0:
            LEDS_ON(BSP_LED_0_MASK | BSP_LED_1_MASK);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            LEDS_OFF(BSP_LED_0_MASK | BSP_LED_1_MASK);
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 15)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(gLedNum, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}
/////////////////////////////////////////////////////////////////////////////////////
void led_blink_kaiji(void)
{
    mTethysLEDstate = LED_DH_kaiji;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(50, 0), NULL);
}

static void resolve_blink_kaiji(void)
{
    #define DELAY_TIME_OFF 950
    #define DELAY_TIME_ON 50

    switch (mTethysDH)
    {
        case 0:
            //LEDS_ON(LEDS_MASK);
            open_LED(4);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            //LEDS_OFF(LEDS_MASK);
            close_LED(4);
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 4)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}
//////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////
/*
 * When bluetooth connected.
 */
void led_blink_connected(void)
{
    mTethysLEDstate = LED_DH_connected;
    mTethysLEDcount = 0;
    mTethysDH = 0;
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(50, 0), NULL);
}

static void resolve_blink_connected(void)
{
    #define DELAY_TIME_OFF 110
    #define DELAY_TIME_ON 10

    switch (mTethysDH)
    {
        case 0:
            LEDS_ON(BSP_LED_0_MASK | BSP_LED_1_MASK);
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_ON, 0), NULL);
            mTethysDH = 1;
            break;
        case 1:
            LEDS_OFF(BSP_LED_0_MASK | BSP_LED_1_MASK);
            mTethysDH = 0;
            mTethysLEDcount++;
            if (mTethysLEDcount > 4)
            {
                break;
            }
            app_timer_start(m_led_timer_id, APP_TIMER_TICKS(DELAY_TIME_OFF, 0), NULL);
            break;
        default:
            break;
    }
    #undef DELAY_TIME_OFF
    #undef DELAY_TIME_ON
}

//////////////////////////////////////////////////////////////////////////////////////
void led_Softblink_start(void)
{
    leda_led_softblink_init(LEDS_MASK);
    led_softblink_start(LEDS_MASK);
    app_timer_start(m_softled_timer_id, APP_TIMER_TICKS(4500, 0), NULL);
}

void led_Softblink_stop(void)
{
    led_softblink_uninit();
}

static void resolve_Softblink_kaiji(void)
{
    led_Softblink_stop();
}

static void leda_led_softblink_init(uint32_t mask)
{
    ret_code_t           err_code;
    led_sb_init_params_t led_sb_init_params = LED_SB_INIT_DEFAULT_PARAMS(mask);
    led_sb_init_params.active_high = true;
    led_sb_init_params.duty_cycle_min = 0;
    led_sb_init_params.duty_cycle_max = 254;
    led_sb_init_params.duty_cycle_step = 4;
    led_sb_init_params.off_time_ticks = 25536;
    led_sb_init_params.on_time_ticks = 1;
    err_code = led_softblink_init(&led_sb_init_params);
    APP_ERROR_CHECK(err_code);
}
//////////////////////////////////////////////////////////////////////////////////////
void led_Softblink_charge_start(uint32_t ledmask)
{
    leda_led_softblink_init(ledmask);
    led_softblink_start(ledmask);
}

void led_Softblink_charge_stop(void)
{
    led_softblink_uninit();
}
//////////////////////////////////////////////////////////////////////////////////////

#ifdef TETHYS_PWM
//////////////////////////////////////////////////////////////////////////////////////
APP_PWM_INSTANCE(PWM1, 1);
static void pwm_ready_callback(uint32_t pwm_id)    // PWM callback function
{
    //ready_flag = true;
}

static int32_t map(int32_t x, int32_t in_min, int32_t in_max, int32_t out_min, int32_t out_max)
{
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}

void led_pwm_init_1(void)
{
    app_pwm_config_t pwm1_cfg = APP_PWM_DEFAULT_CONFIG_1CH(256L, LED_1);
    
    /* Switch the polarity of the second channel. */
    //pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_HIGH;
    pwm1_cfg.pin_polarity[1] = APP_PWM_POLARITY_ACTIVE_LOW;

    /* Initialize PWM. */
    uint32_t err_code = app_pwm_init(&PWM1, &pwm1_cfg, pwm_ready_callback);
    APP_ERROR_CHECK(err_code);
    //app_pwm_disable(&PWM1);
}

void led_pwm_start_1(void)
{
    mTethysLEDstate = LED_DH_BREATH;
    millis = 100;
    gPWMstop = false;
    #if 0
    LEDS_ON(LEDS_ROL_MASK);
    LEDS_OFF(LEDS_COL_MASK);

    LEDS_ON(LEDS_COL_MASK);
    #endif
    app_pwm_enable(&PWM1);
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(100, 0), NULL);
    
    //app_pwm_channel_duty_set(&PWM1, 0, 1);
    //while (app_pwm_channel_duty_set(&PWM1, 0, 1) == NRF_ERROR_BUSY);
}

void led_pwm_stop_1(void)
{
    app_pwm_disable(&PWM1);
    gPWMstop = true;
    mTethysLEDstate = LED_DH_NOTHING;
    close_LED(4);
}

static void resolve_pwm_1(void)
{
    double val = (exp(sin((double)millis/3500.0*2.5)) - 0.36787944)*100;
    //app_pwm_duty_t duty = map(val, 0, 255, 0, 2040);
    app_pwm_duty_t duty = val;
//    app_pwm_channel_duty_set(&PWM1, 0, duty);
    while (app_pwm_channel_duty_set(&PWM1, 0, duty) == NRF_ERROR_BUSY);
    millis += 4;
    //millis++;
    if (millis > 300000)
    {
        led_pwm_stop_1();
        return;
    }
    app_timer_start(m_led_timer_id, APP_TIMER_TICKS(1, 0), NULL);
}
#endif
