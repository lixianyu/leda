/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "boards.h"
#include "nrf_drv_gpiote.h"
#include "nrf_soc.h"
#include "nrf_adc.h"
#include "ble_gap.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "tethys_led.h"
#include "leda_pmu.h"
#include "tethys_battery.h"
#include "mimas_config.h"
#include "leda_communicate_protocol.h"

extern bool gLedaIsTransmittingBin;
extern bool gLedaIsUpdattingAVR;
extern uint16_t g_battery_voltage_mv;
extern void advertising_start(uint8_t type);

static void init_IO_VBUS_pin(void);
static void uninit_IO_VBUS_pin(void);
static void open_IO_PULLUP_PIN(void);
static void close_IO_PULLUP_PIN(void);
static void vbus_timer_handler(void *p_context);
static void IO_VBUS_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
static void IO_CHRG_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
static void IO_VDD_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
static void pmu_timer_handler(void *p_context);
static void handle_pmu_timer(void);
static void handle_IO_CHRG_event(void);
static void handle_IO_VDD_event(void);

/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
APP_TIMER_DEF(m_pmu_time_id);
APP_TIMER_DEF(m_vbus_check_time_id);

#define POWER_LED 3 //Red
#define CHRG_LED 4 //Green
#define CHRG_LED_MASK BSP_LED_3_MASK

uint8_t gIfUSBIn = 0;// 1, VBus voltage<=2400mv; 2, VBus voltage>2400mv
bool gIfCheckVBUS = true;

/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
void init_leda_pmu(void)
{
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;

    gIfUSBIn = 0;
    gIfCheckVBUS = true;
    if (!nrf_drv_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        if (err_code != NRF_SUCCESS)
        {
            return;
        }
    }
    //init_IO_VBUS_pin();
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    err_code = nrf_drv_gpiote_in_init(IO_VDD_PIN, &in_config, IO_VDD_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_VDD_PIN, true);
    nrf_gpio_cfg(
        IO_SWTTL_PIN,
        NRF_GPIO_PIN_DIR_OUTPUT,
        NRF_GPIO_PIN_INPUT_DISCONNECT,
        NRF_GPIO_PIN_NOPULL,
        NRF_GPIO_PIN_S0S1,
        NRF_GPIO_PIN_NOSENSE);

    app_timer_create(&m_pmu_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     pmu_timer_handler);
    app_timer_create(&m_vbus_check_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     vbus_timer_handler);
    app_timer_start(m_pmu_time_id, APP_TIMER_TICKS(500, 0), NULL);
}

static void IO_CHRG_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_CHRG_event);
}

static void IO_VBUS_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    if (nrf_drv_gpiote_in_is_set(IO_VBUS_PIN))
    {
        gIfUSBIn = true;
        g_battery_voltage_mv = BATTERY_FULL;
        uninit_IO_VBUS_pin();
        //nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_5); // For USB voltage check
        //app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(2500, 0), NULL);
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VBUS_event);
    }
}

static void init_IO_VBUS_pin(void)
{
    #if 0
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    uint32_t err_code = nrf_drv_gpiote_in_init(IO_VBUS_PIN, &in_config, IO_VBUS_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_VBUS_PIN, true);
    #else
    nrf_gpio_cfg_input(IO_VBUS_PIN, NRF_GPIO_PIN_NOPULL);
    #endif
}

static void uninit_IO_VBUS_pin(void)
{
    #if 0
    nrf_drv_gpiote_in_uninit(IO_VBUS_PIN);
    #else
    #endif
}

static void init_IO_CHRG_pin(void)
{
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    uint32_t err_code = nrf_drv_gpiote_in_init(IO_CHRG_PIN, &in_config, IO_CHRG_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_CHRG_PIN, true);
}

static void uninit_IO_CHRG_pin(void)
{
    nrf_drv_gpiote_in_uninit(IO_CHRG_PIN);
}

static inline bool pmu_if_leda_machine_open(void)
{
    if (!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
    {
        return false;
    }
    return true;
}

void leda_system_off(void)
{
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_DISABLED);
    close_LEDS();
    uint32_t err_code;

    //APP_ERROR_HANDLER(1);
    
    
    uint32_t new_cnf = NRF_GPIO->PIN_CNF[IO_VDD_PIN];
    uint32_t new_sense = GPIO_PIN_CNF_SENSE_High;
    new_cnf &= ~GPIO_PIN_CNF_SENSE_Msk;
    new_cnf |= (new_sense << GPIO_PIN_CNF_SENSE_Pos);
    NRF_GPIO->PIN_CNF[IO_VDD_PIN] = new_cnf;

    new_cnf = NRF_GPIO->PIN_CNF[IO_VBUS_PIN];
    new_sense = GPIO_PIN_CNF_SENSE_High;
    new_cnf &= ~GPIO_PIN_CNF_SENSE_Msk;
    new_cnf |= (new_sense << GPIO_PIN_CNF_SENSE_Pos);
    NRF_GPIO->PIN_CNF[IO_VBUS_PIN] = new_cnf;

    
    sd_power_system_off();
}

void handle_usb_state_change(void)
{
    if (gIfUSBIn == 1) // USB is out
    {
        nrf_gpio_pin_clear(IO_SWTTL_PIN);
        led_Softblink_charge_stop();
        close_LED(CHRG_LED);
        if (pmu_if_leda_machine_open())
        {
            close_IO_PULLUP_PIN();
            uninit_IO_CHRG_pin();
            advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
        }
        else
        {
            init_IO_VBUS_pin();
            leda_system_off();
        }
    }
    else if (gIfUSBIn == 2) // USB is in
    {
        if (pmu_if_leda_machine_open())
        {
            nrf_gpio_pin_set(IO_SWTTL_PIN);
            led_blink_stop();
            open_LED(POWER_LED);
            open_IO_PULLUP_PIN();
            init_IO_CHRG_pin();
            handle_IO_CHRG_event();
            app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(1000, 0), NULL);
        }
    }

}

void handle_IO_LOWBAT_event(void)
{
    gIfCheckVBUS = true;
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_5);
    if (gIfUSBIn == 2) //USB插入
    {
        return;
    }
    if (!tethys_if_low_battery())//有电
    {
        led_blink_stop();
        if(!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {
            //电源LED熄灭
            close_LED(POWER_LED);
        }
        else//开机状态
        {
            //电源LED常亮
            open_LED(POWER_LED);
        }
    }
    else//低电量
    {
        if(!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {
            //电源LED熄灭
            led_blink_stop();
            close_LED(POWER_LED);
        }
        else//开机状态
        {
            //电源LED闪烁提示低电量
            led_blink(POWER_LED);
        }
    }
}

void pmu_check_battery(void)
{
    gIfCheckVBUS = false;
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_3); // For battery voltage check.
}

static void handle_IO_CHRG_event(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_CHRG_PIN))//充满
    {
        //充电LED常亮提示充满
        led_Softblink_charge_stop();
        open_LED(CHRG_LED);
    }
    else//充电中
    {
        //充电LED闪烁提示充电状态
        led_Softblink_charge_start(CHRG_LED_MASK);
    }
}

static void open_IO_PULLUP_PIN(void)
{
    nrf_gpio_cfg(
        IO_PULLUP_PIN,
        NRF_GPIO_PIN_DIR_OUTPUT,
        NRF_GPIO_PIN_INPUT_DISCONNECT,
        NRF_GPIO_PIN_PULLUP,
        NRF_GPIO_PIN_S0S1,
        NRF_GPIO_PIN_NOSENSE);
    nrf_gpio_pin_set(IO_PULLUP_PIN);
}

static void close_IO_PULLUP_PIN(void)
{
    nrf_gpio_cfg(
        IO_PULLUP_PIN,
        NRF_GPIO_PIN_DIR_INPUT,
        NRF_GPIO_PIN_INPUT_CONNECT,
        NRF_GPIO_PIN_PULLUP,
        NRF_GPIO_PIN_S0S1,
        NRF_GPIO_PIN_NOSENSE);
}

static void handle_pmu_timer(void)
{
    if (gIfUSBIn == 0)
    {
        //nrf_adc_start();
        //gIfUSBIn = 1;
        app_timer_start(m_pmu_time_id, APP_TIMER_TICKS(200, 0), NULL);// Try again.
        return;
    }
    if (pmu_if_leda_machine_open())
    {
        //nrf_adc_start();
        open_LED(POWER_LED);
        if (gIfUSBIn == 1) // vbus out
        {
            pmu_check_battery();
            uninit_IO_VBUS_pin();
            uninit_IO_CHRG_pin();
            close_IO_PULLUP_PIN();
            advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
        }
#if 0
        else if (gIfUSBIn == 2)// vbus in
        {
            nrf_gpio_pin_set(IO_SWTTL_PIN);
        }
#endif
        app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(1000, 0), NULL);
    }
    else
    {
        close_LED(POWER_LED);
        nrf_gpio_pin_clear(IO_SWTTL_PIN);
        if (gIfUSBIn == 1) // vbus out
        {
            init_IO_VBUS_pin();
            leda_system_off();
            return;
        }
        else if (gIfUSBIn == 2) // vbus in
        {
            #if 1
            uninit_IO_VBUS_pin();
            open_IO_PULLUP_PIN();
            init_IO_CHRG_pin();
            handle_IO_CHRG_event();
            app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(1000, 0), NULL);
            #else
            //APP_ERROR_HANDLER(1);
            #endif
        }
    }
}

static void pmu_timer_handler(void *p_context)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_pmu_timer);
}

static void vbus_timer_handler(void *p_context)
{
    static uint8_t cout = 1;
    if (gLedaIsUpdattingAVR)// 如果nRF51822正在给m328p升级程序
    {
        app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(5000, 0), NULL);
        return;
    }
    if (cout > 5)
    {
        cout = 1;
        pmu_check_battery();
    }
    else
    {
        cout++;
    }
    nrf_adc_start();
    app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(1000, 0), NULL);
}

static void handle_IO_VDD_event(void)
{
    if (pmu_if_leda_machine_open())
    {
        if (gIfUSBIn == 2) // vbus in
        {
            led_blink_stop();
            open_LED(POWER_LED);
            nrf_gpio_pin_set(IO_SWTTL_PIN);
#if 1
            open_IO_PULLUP_PIN();
            init_IO_CHRG_pin();
            handle_IO_CHRG_event();
#endif
        }
        advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
    }
    else
    {
        if (gIfUSBIn == 1) // vbus out
        {
            init_IO_VBUS_pin();
            nrf_gpio_pin_clear(IO_SWTTL_PIN);
            leda_uart_uninit();
            led_blink_stop();
            close_LEDS();
            uint32_t err_code = sd_ble_gap_adv_stop();
            //APP_ERROR_CHECK(err_code);
            leda_system_off();
        }
        else if (gIfUSBIn == 2) // vbus in
        {
            //led_Softblink_charge_stop();
            //close_LED(CHRG_LED);
#if 1
            open_IO_PULLUP_PIN();
            init_IO_CHRG_pin();
            handle_IO_CHRG_event();
#endif
            led_blink_stop();
            close_LED(POWER_LED);
            nrf_gpio_pin_clear(IO_SWTTL_PIN);
            sd_ble_gap_adv_stop();
        }
    }
}

static void IO_VDD_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VDD_event);
}

bool pmu_is_charging(void)
{
    if (gIfUSBIn) //USB插入
    {
        return true;
    }
    return false;
}

