/******************************************************************************/
/***************************** Include Files **********************************/
/******************************************************************************/
#include "boards.h"
#include "nrf_drv_gpiote.h"
#include "nrf_soc.h"
#include "nrf_adc.h"
#include "ble_gap.h"
#include "app_timer.h"
#include "app_scheduler.h"
#include "tethys_led.h"
#include "leda_pmu.h"
#include "tethys_battery.h"
#include "mimas_config.h"
#include "leda_communicate_protocol.h"

extern uint16_t g_battery_voltage_mv;
extern void advertising_start(uint8_t type);

static void init_IO_VBUS_pin(void);
static void vbus_timer_handler(void *p_context);
static void TTL_timer_handler(void *p_context);
static void IO_VBUS_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
static void IO_CHRG_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
static void IO_VDD_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
#ifdef LEDA_LOWBAT_IO
static void IO_LOWBAT_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action);
#endif
static void pmu_timer_handler(void *p_context);
static void handle_pmu_timer(void);
static void handle_IO_VBUS_event(void);
static void handle_IO_CHRG_event(void);
static void handle_IO_VDD_event(void);
static void set_pin_no_work(uint32_t pin_number);
/******************************************************************************/
/************************* Variables Declarations *****************************/
/******************************************************************************/
APP_TIMER_DEF(m_pmu_time_id);
APP_TIMER_DEF(m_vbus_check_time_id);
APP_TIMER_DEF(m_TTL_check_time_id);
bool gIfUSBIn = false;

#define POWER_LED 3 //Red
#define CHRG_LED 4 //Green
#define CHRG_LED_MASK BSP_LED_3_MASK
/******************************************************************************/
/************************ Functions Definitions *******************************/
/******************************************************************************/
//#define PIN_IS_ON(pins_mask) ((pins_mask) & (NRF_GPIO->OUT ^ LEDS_INV_MASK) )
#if 0
__STATIC_INLINE uint32_t nrf_gpio_pin_state(uint32_t pin_number)
{
    uint32_t pin_state = ((NRF_GPIO->OUT >> pin_number) & 1UL);
    return pin_state;
}
#endif

void init_leda_pmu(void)
{
    uint32_t err_code;
    nrf_drv_gpiote_in_config_t in_config;

    gIfUSBIn = false;
    if (!nrf_drv_gpiote_is_init())
    {
        err_code = nrf_drv_gpiote_init();
        if (err_code != NRF_SUCCESS)
        {
            return;
        }
    }
    init_IO_VBUS_pin();
    
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    err_code = nrf_drv_gpiote_in_init(IO_VDD_PIN, &in_config, IO_VDD_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_VDD_PIN, true);

    nrf_gpio_cfg(
            IO_SWTTL_PIN,
            NRF_GPIO_PIN_DIR_OUTPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_NOPULL,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
    //nrf_gpio_pin_set(IO_SWTTL_PIN);

    app_timer_create(&m_TTL_check_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     TTL_timer_handler);
    app_timer_create(&m_vbus_check_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     vbus_timer_handler);
    
    app_timer_create(&m_pmu_time_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     pmu_timer_handler);
    app_timer_start(m_pmu_time_id, APP_TIMER_TICKS(500, 0), NULL);
    //app_timer_start(m_TTL_check_time_id, APP_TIMER_TICKS(2000, 0), NULL);
}

void leda_start_pmu_vbus_check(void)
{
    app_timer_start(m_pmu_time_id, APP_TIMER_TICKS(2100, 0), NULL);
}

static void init_IO_VBUS_pin(void)
{
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    uint32_t err_code = nrf_drv_gpiote_in_init(IO_VBUS_PIN, &in_config, IO_VBUS_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_VBUS_PIN, true);
}

static void uninit_IO_VBUS_pin(void)
{
    nrf_drv_gpiote_in_uninit(IO_VBUS_PIN);
}

static void init_IO_CHRG_pin(void)
{
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_PULLDOWN;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    uint32_t err_code = nrf_drv_gpiote_in_init(IO_CHRG_PIN, &in_config, IO_CHRG_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_CHRG_PIN, true);
}

static void uninit_IO_CHRG_pin(void)
{
    nrf_drv_gpiote_in_uninit(IO_CHRG_PIN);
}

static void init_IO_LOWBAT_pin(void)
{
#ifdef LEDA_LOWBAT_IO
    nrf_drv_gpiote_in_config_t in_config;
    in_config.is_watcher = false;
    in_config.hi_accuracy = false;
    in_config.pull = NRF_GPIO_PIN_NOPULL;
    in_config.sense = NRF_GPIOTE_POLARITY_TOGGLE;
    uint32_t err_code = nrf_drv_gpiote_in_init(IO_LOWBAT_PIN, &in_config, IO_LOWBAT_pin_handler);
    APP_ERROR_CHECK(err_code);
    nrf_drv_gpiote_in_event_enable(IO_LOWBAT_PIN, true);
#endif
}

static void uninit_IO_LOWBAT_pin(void)
{
#ifdef LEDA_LOWBAT_IO
    nrf_drv_gpiote_in_uninit(IO_LOWBAT_PIN);
#endif
}

static void init_IO_VBAT_pin(void)
{//ADC init.
    
}

static void uninit_IO_VBAT_pin(void)
{
    //set_pin_no_work(IO_VBAT_PIN);
}

static void pmu_timer_handler(void *p_context)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_pmu_timer);
}

static void vbus_timer_handler(void *p_context)
{
    nrf_adc_start();
    app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(1500, 0), NULL);
}

static void TTL_timer_handler(void *p_context)
{
    static bool bFlag = true;
    if (bFlag)
    {
        bFlag = false;
        open_LED(1);
    }
    else
    {
        bFlag = true;
        close_LED(1);
    }
    #if 0
    if (!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
    {
        nrf_gpio_pin_clear(IO_SWTTL_PIN);
    }
    else
    {
        if (gIfUSBIn)
        {
            nrf_gpio_pin_set(IO_SWTTL_PIN);
        }
        else
        {
            nrf_gpio_pin_clear(IO_SWTTL_PIN);
        }
    }
    #else
    nrf_gpio_pin_set(IO_SWTTL_PIN);
    #endif
    app_timer_start(m_TTL_check_time_id, APP_TIMER_TICKS(2000, 0), NULL);
}

static void handle_pmu_timer(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_VBUS_PIN))
    {
        gIfUSBIn = true;
        uninit_IO_VBUS_pin();
        nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_5); // For USB voltage check
        app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(2500, 0), NULL);
    }
    else
    {
        gIfUSBIn = false;
        nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_3); // For battery 
    }
    //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VBUS_event);
    handle_IO_VBUS_event();
}

static void IO_VBUS_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    if (nrf_drv_gpiote_in_is_set(IO_VBUS_PIN))
    {
        gIfUSBIn = true;
        g_battery_voltage_mv = BATTERY_FULL;
        uninit_IO_VBUS_pin();
        nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_5); // For USB voltage check
        app_timer_start(m_vbus_check_time_id, APP_TIMER_TICKS(2500, 0), NULL);
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VBUS_event);
    }
    #if 0
    else
    {
        gIfUSBIn = false;
        nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_3); // For battery
        app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VBUS_event);
    }
    #endif
    
}

//相当于低电
void IO_VBUS_pin_handler_adc(void)
{
    app_timer_stop(m_vbus_check_time_id);
    nrf_adc_input_select(NRF_ADC_CONFIG_INPUT_3); // For battery
    init_IO_VBUS_pin();
    gIfUSBIn = false;
    handle_IO_VBUS_event();
}

static void IO_CHRG_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_CHRG_event);
}

static void IO_VDD_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_VDD_event);
}

#ifdef LEDA_LOWBAT_IO
static void IO_LOWBAT_pin_handler(nrf_drv_gpiote_pin_t pin, nrf_gpiote_polarity_t action)
{
    app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_LOWBAT_event);
}
#endif

static void set_pin_no_work(uint32_t pin_number)
{
    nrf_gpio_cfg_default(pin_number);
}

bool pmu_is_charging(void)
{
    if (gIfUSBIn) //USB插入
    {
        return true;
    }
    return false;
}

static void handle_IO_VBUS_event(void)
{
    if (gIfUSBIn) //USB插入
    {//此时无用的口是IO_VBAT_PIN, IO_LOWBAT_PIN
        nrf_gpio_cfg(
            IO_PULLUP_PIN,
            NRF_GPIO_PIN_DIR_OUTPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
        nrf_gpio_pin_set(IO_PULLUP_PIN);
        nrf_gpio_pin_set(IO_SWTTL_PIN);
        init_IO_CHRG_pin();
        //uninit_IO_LOWBAT_pin();
        //uninit_IO_VBAT_pin();
        
        handle_IO_VDD_event();
        handle_IO_CHRG_event();
        //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)handle_IO_CHRG_event);
    }
    else //USB拔出
    {//此时无用的口是IO_CHRG_PIN
        close_LED(CHRG_LED);
        nrf_gpio_pin_clear(IO_SWTTL_PIN);
    #ifdef LEDA_SOFTBLINK
        led_Softblink_charge_stop();
    #endif
        uninit_IO_CHRG_pin();
        nrf_gpio_cfg(
            IO_PULLUP_PIN,
            NRF_GPIO_PIN_DIR_INPUT,
            NRF_GPIO_PIN_INPUT_CONNECT,
            NRF_GPIO_PIN_PULLUP,
            NRF_GPIO_PIN_S0S1,
            NRF_GPIO_PIN_NOSENSE);
        //init_IO_VBAT_pin();
        //init_IO_LOWBAT_pin();
        if (!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//如果是关机态
        {
            leda_system_off();
            return;
        }
        else
        {
            advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
        }
        handle_IO_LOWBAT_event();
    }
}

static void handle_IO_CHRG_event(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_CHRG_PIN))//充满
    {//充电LED常亮提示充满
    #ifdef LEDA_SOFTBLINK
        led_Softblink_charge_stop();
    #else
        led_blink_stop();
    #endif
        open_LED(CHRG_LED);
    }
    else//充电中
    {//充电LED闪烁提示充电状态
    #ifdef LEDA_SOFTBLINK
        led_Softblink_charge_start(CHRG_LED_MASK);
    #else
        led_blink(CHRG_LED);
    #endif
    }
}

static void handle_IO_VDD_event(void)
{
    if (gIfUSBIn)//如果USB是插入态
    {
    #if (LEDA_HARDWARE_REVISION == 2)
        if (!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
    #else
        if (nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
    #endif
        {//电源LED熄灭
            close_LED(POWER_LED);
            nrf_gpio_pin_clear(IO_SWTTL_PIN);
            sd_ble_gap_adv_stop();
        }
        else//开机状态
        {//电源LED常亮
            nrf_gpio_pin_set(IO_SWTTL_PIN);
            led_blink_stop();
            if (leda_if_connected() == false)
            {
                advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
            }
            open_LED(POWER_LED);
        }
    }
    else
    {
        nrf_gpio_pin_clear(IO_SWTTL_PIN);
        if (!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//如果是关机态
        {
            leda_uart_uninit();
            led_blink_stop();
            close_LEDS();
            leda_system_off();
            return;
        }
        else
        {
            advertising_start(BLE_GAP_ADV_TYPE_ADV_NONCONN_IND);
        }
        handle_IO_LOWBAT_event();
    }
}

#ifdef LEDA_LOWBAT_IO
void handle_IO_LOWBAT_event(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_LOWBAT_PIN))//有电
    {
        led_blink_stop();
        if(nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED常亮
            open_LED(POWER_LED);
        }
    }
    else//低电量
    {
        //open_LED(1);
        //open_LED(2);
        if(nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            led_blink_stop();
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED闪烁提示低电量
            led_blink(POWER_LED);
            //led_pwm_start_1();
        }
    }
}
#else
void handle_IO_LOWBAT_event(void)
{
    if (gIfUSBIn) //USB插入
    {
        return;
    }
    if (!tethys_if_low_battery())//有电
    {
        led_blink_stop();
    #if (LEDA_HARDWARE_REVISION == 2)
        if(!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
    #else
        if(nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
    #endif
        {//电源LED熄灭
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED常亮
            open_LED(POWER_LED);
        }
    }
    else//低电量
    {
        if(!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            led_blink_stop();
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED闪烁提示低电量
            led_blink(POWER_LED);
            //led_pwm_start_1();
        }
    }
}
#endif

#if 0
void handle_HIGH_BAT_event(void)
{
    if (nrf_drv_gpiote_in_is_set(IO_VBUS_PIN)) //USB插入
    {
        led_blink_stop();
        if(!nrf_drv_gpiote_in_is_set(IO_VDD_PIN))//关机状态
        {//电源LED熄灭
            close_LED(POWER_LED);
        }
        else//开机状态
        {//电源LED常亮
            open_LED(POWER_LED);
        }
    }
}
#endif

void leda_system_off(void)
{
    close_LEDS();
    uint32_t err_code;

    err_code = sd_ble_gap_adv_stop();
    APP_ERROR_CHECK(err_code);

    uint32_t new_cnf = NRF_GPIO->PIN_CNF[IO_VDD_PIN];
    uint32_t new_sense = GPIO_PIN_CNF_SENSE_High;
    new_cnf &= ~GPIO_PIN_CNF_SENSE_Msk;
    new_cnf |= (new_sense << GPIO_PIN_CNF_SENSE_Pos);
    NRF_GPIO->PIN_CNF[IO_VDD_PIN] = new_cnf;

    new_cnf = NRF_GPIO->PIN_CNF[IO_VBUS_PIN];
    new_sense = GPIO_PIN_CNF_SENSE_High;
    new_cnf &= ~GPIO_PIN_CNF_SENSE_Msk;
    new_cnf |= (new_sense << GPIO_PIN_CNF_SENSE_Pos);
    NRF_GPIO->PIN_CNF[IO_VBUS_PIN] = new_cnf;

    sd_power_system_off();
}

