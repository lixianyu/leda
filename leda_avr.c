#include <stdint.h>
#include "string.h"

#include "mimas_bsp.h"
#include "config.h"
#include "leda_communicate_protocol.h"
#include "nrf_error.h"
#include <app_scheduler.h>
#include "app_error.h"
#include "app_timer.h"
#include "nrf_drv_rng.h"
#include "mimas_log.h"
#include "mimas_ble_flash.h"
#include "ble_hci.h"
#include "ble_conn_params.h"
#include "app_trace.h"

#include "mimas_config.h"
#include "tethys_led.h"
#include "leda_nrf_drv_uart.h"
#include "app_uart.h"
#include "main_common.h"
#include "leda_file.h"

extern bool gEndByApp;
extern bool gLedaIsUpdattingAVR;
extern bool gLedaIsTransmittingBin;
extern bool gHaveUpdateCoonParamToMin;
extern ble_nus_t m_nus;
extern uint8_t g_pstorage_buffer[32];

extern void avr_write_elara_2_finished(void);
extern void leda_long_conn_param(void);

static void UpdateAVRevent(void);
uint32_t avr_uart_init(uint32_t baudrate);

typedef enum
{
    m328p_16M_5V = 0,//Upload speed is 115200
    m328p_8M_3_3V,   //Upload speed is 57600

    m644p_16M_5V,    //Upload speed is 115200
    m644p_8M_3_3V,   //Upload speed is 57600

    m1284p_16M_5V,   //Upload speed is 115200
    m1284p_8M_3_3V,  //Upload speed is 57600

    m32u4_16M,       //Upload speed is 57600
    m128rfa1_16M,     //Upload speed is 57600

    m128rfr2,
    m64rfr2,
    m2560,
    m2561,

    m328_16M_5V,     //Upload speed is 115200
    m328_8M_3_3V,    //Upload speed is 57600
    m644_16M_5V,     //Upload speed is 115200
    m644_8M_3_3V,    //Upload speed is 57600
    m1284_16M_5V,    //Upload speed is 115200
    m1284_8M_3_3V,   //Upload speed is 57600
} t_part_id;

typedef enum
{
    UPDATE_AVR_IDLE = 0,
    UPDATE_AVR_CONN_PARAM_UPDATE,
    UPDATE_AVR_INIT,
    UPDATE_AVR_INIT_WRITE_DONE,
    UPDATE_AVR_INIT_UART,
    UPDATE_AVR_INIT_AVR,
    UPDATE_AVR_STATE_2,
    UPDATE_AVR_STATE_3,
    UPDATE_AVR_STATE_4,
    UPDATE_AVR_STATE_5,
    UPDATE_AVR_FINISHED,
} UpdateAvrState;
static UpdateAvrState gUAS = UPDATE_AVR_IDLE;

//static uint8_t rx_buffer[1];
APP_TIMER_DEF(m_avr_timer_id);
// For using app_timer_cnt_get function, a dummy timer should be run.
APP_TIMER_DEF(m_rx_time_out_id);
uint8_t gPartID = m328p_16M_5V;

static void avr_timeout_handler(void *p_context)
{
    UpdateAVRevent();
    //app_sched_event_put(NULL, 0, (app_sched_event_handler_t)UpdateAVRevent);
}

static void rx_timeout_handler(void *p_context)
{
    //invert_LED(3);
    //nrf_drv_uart_rx_abort();
}

void init_avr(void)
{
    app_timer_create(&m_avr_timer_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     avr_timeout_handler);
    app_timer_create(&m_rx_time_out_id,
                     APP_TIMER_MODE_SINGLE_SHOT,
                     rx_timeout_handler);
    
    //app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(99, 0), NULL);
    elara_flash_write_init();
    
    //avr_uart_init(UART_BAUDRATE_BAUDRATE_Baud115200);
}

#if 1
uint32_t avr_uart_init(uint32_t baudrate)
{
    uint32_t err_code = NRF_SUCCESS;
    app_uart_comm_params_t comm_params =
    {
        LEDA_RX_PIN,
        LEDA_TX_PIN,
        0xFF,
        0xFF,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        baudrate//UART_BAUDRATE_BAUDRATE_Baud115200
    };
#if 0
    APP_UART_FIFO_INIT( &comm_params,
                        FINGERPRINT_UART_RX_BUF_SIZE,
                        FINGERPRINT_UART_TX_BUF_SIZE,
                        fingerprint_uart_handle,
                        APP_IRQ_PRIORITY_LOW,
                        err_code);
#endif
    app_uart_comm_params_t * p_comm_params = &comm_params;
    nrf_drv_uart_config_t config;
    config.baudrate = (nrf_uart_baudrate_t)p_comm_params->baud_rate;
    config.hwfc = (p_comm_params->flow_control == APP_UART_FLOW_CONTROL_DISABLED) ?
            NRF_UART_HWFC_DISABLED : NRF_UART_HWFC_ENABLED;
    config.interrupt_priority = APP_IRQ_PRIORITY_LOW;
    config.parity = p_comm_params->use_parity ? NRF_UART_PARITY_INCLUDED : NRF_UART_PARITY_EXCLUDED;
    config.pselcts = p_comm_params->cts_pin_no;
    config.pselrts = p_comm_params->rts_pin_no;
    config.pselrxd = p_comm_params->rx_pin_no;
    config.pseltxd = p_comm_params->tx_pin_no;

    err_code = nrf_drv_uart_init(&config, NULL);

    if (err_code != NRF_SUCCESS)
    {
        return err_code;
    }

    nrf_drv_uart_rx_enable();
    //return nrf_drv_uart_rx_avr(rx_buffer, 1);
    return 0;
}

// For using app_timer_cnt_get function, a dummy timer should be run.
void avr_uart_rx_timeout_start(void)
{
    app_timer_start(m_rx_time_out_id, APP_TIMER_TICKS(300000, 0), NULL);//5 minutes.
}

void avr_uart_rx_timeout_stop(void)
{
    app_timer_stop(m_rx_time_out_id);
}

void avr_uart_uninit(void)
{
    nrf_drv_uart_uninit();
#if 1
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
}
#else
/**@brief   Function for handling app_uart events.
 *
 * @details This function will receive a single character from the app_uart module and append it to 
 *          a string. The string will be be sent over BLE when the last character received was a 
 *          'new line' i.e '\n' (hex 0x0D) or if the string has reached a length of 
 *          @ref NUS_MAX_DATA_LENGTH.
 */
/**@snippet [Handling the data received over UART] */
static void avr_uart_event_handle(app_uart_evt_t * p_event)
{
    static uint8_t data_array[BLE_NUS_MAX_DATA_LEN];
    static uint8_t index = 0;
    uint32_t       err_code;

    switch (p_event->evt_type)
    {
        case APP_UART_DATA_READY:
            //invert_LED(4);
            open_LED(1);
            #if 0
            UNUSED_VARIABLE(app_uart_get(&data_array[index]));
            index++;

            if ((data_array[index - 1] == '\n') || (index >= (BLE_NUS_MAX_DATA_LEN)))
            {
                err_code = tethys_ble_nus_string_send(&m_tethys_nus, data_array, index);
                if (err_code != NRF_ERROR_INVALID_STATE)
                {
                    APP_ERROR_CHECK(err_code);
                }
                
                index = 0;
            }
            #endif
            break;

        case APP_UART_COMMUNICATION_ERROR:
            open_LED(2);//Green
            //APP_ERROR_HANDLER(p_event->data.error_communication);
            break;

        case APP_UART_FIFO_ERROR:
            open_LED(3);//Green LED
            //APP_ERROR_HANDLER(p_event->data.error_code);
            break;

        case APP_UART_TX_EMPTY:
            open_LED(4);
            break;

        case APP_UART_DATA:
            open_LED(5);
            break;
        default:
            break;
    }
}
#define AVR_UART_RX_BUF_SIZE    256
#define AVR_UART_TX_BUF_SIZE    256
uint32_t avr_uart_init(uint32_t baudrate)
{
    uint32_t err_code = NRF_SUCCESS;
    const app_uart_comm_params_t comm_params =
    {
        LEDA_RX_PIN,
        LEDA_TX_PIN,
        0xFF,
        0xFF,
        APP_UART_FLOW_CONTROL_DISABLED,
        false,
        UART_BAUDRATE_BAUDRATE_Baud115200
    };

    APP_UART_FIFO_INIT( &comm_params,
                        AVR_UART_RX_BUF_SIZE,
                        AVR_UART_TX_BUF_SIZE,
                        avr_uart_event_handle,
                        APP_IRQ_PRIORITY_HIGH,
                        err_code);
    APP_ERROR_CHECK(err_code);
    return 0;
}

void avr_uart_uninit(void)
{
    app_uart_close();
#if 1
    NRF_UART0->TASKS_STOPTX = 1;
    NRF_UART0->TASKS_STOPRX = 1;
    NRF_UART0->ENABLE = 0;
#endif
}
#endif

void leda_init_update_FSM(void)
{
    gLedaIsTransmittingBin = false;
    gLedaIsUpdattingAVR = false;
    gEndByApp = false;
    gHaveUpdateCoonParamToMin = false;
    gUAS = UPDATE_AVR_IDLE;
}

void processAVRevent(void)
{
    uint32_t uState = 0;
    //uint8_t ret = elara_snv_read(BLE_NVID_U_STATE, 1, &uState);

    if (uState == 0)//In this state, Leda wait phone handset to connect to download bin
    {
        //open_LED(1);
        //elara_flash_write_init();
        //osal_pwrmgr_device( PWRMGR_ALWAYS_ON );
        if (gUAS != UPDATE_AVR_STATE_4)
        {
            //getBaudAndSignature();
        }
        //gManualTerminate = 0;
        gUAS = UPDATE_AVR_IDLE;

        //osal_start_timerEx( keyfobapp_TaskID, KFD_UART_INIT_EVT, 1000 );
        return;
    }
}

#ifdef UPDATE_AVR_TEST
void update_avr_test(void)
{
    invert_LED(5);
    gUAS = UPDATE_AVR_INIT;
    app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(5000, 0), NULL);
}
#endif

#ifdef LEDA_UPDATE_CONN_PARAM
void update_avr_continue(void)
{
    if (gUAS == UPDATE_AVR_CONN_PARAM_UPDATE)
    {
        gUAS = UPDATE_AVR_INIT;
        app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(1000, 0), NULL);
    }
    else if (gUAS == UPDATE_AVR_IDLE)
    {
        gHaveUpdateCoonParamToMin = true;
    }
}
#endif

void leda_stop_update_avr(void)
{
    app_timer_stop(m_avr_timer_id);
    gLedaIsUpdattingAVR = false;
    avr_write_elara_2_finished();
    nrf_gpio_cfg_default(AVR_RESET_PIN);
    avr_uart_uninit();
    gUAS = UPDATE_AVR_IDLE;
}

void leda_update_avr_pre(void)
{
    LEDS_ON(BSP_LED_0_MASK);
    #if 1
    nrf_gpio_cfg_output(AVR_RESET_PIN);
    #else
    nrf_gpio_cfg(
            AVR_RESET_PIN,
            NRF_GPIO_PIN_DIR_OUTPUT,
            NRF_GPIO_PIN_INPUT_DISCONNECT,
            NRF_GPIO_PIN_NOPULL,
            NRF_GPIO_PIN_S0D1,
            NRF_GPIO_PIN_NOSENSE);
    #endif
    nrf_gpio_pin_clear(AVR_RESET_PIN);
    #ifdef LEDA_UPDATE_CONN_PARAM
    gUAS = UPDATE_AVR_CONN_PARAM_UPDATE;
    #else
    gUAS = UPDATE_AVR_INIT;
    #endif
    app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(1000, 0), NULL);
}

extern int avr_write_elara_2_next(void);
extern void leda_leave_connect_to_debug(uint32_t error_id);
static void UpdateAVRevent(void)
{
    int exitRC = 0;
    if (gUAS == UPDATE_AVR_CONN_PARAM_UPDATE)
    {
        #ifdef LEDA_UPDATE_CONN_PARAM
        leda_long_conn_param();
        #endif
    }
    else if (gUAS == UPDATE_AVR_INIT)
    {
        LEDS_OFF(BSP_LED_0_MASK);
        //avr_uart_rx_timeout_start();
        gUAS = UPDATE_AVR_INIT_WRITE_DONE;
        app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(1000, 0), NULL);
    }
    else if (gUAS == UPDATE_AVR_INIT_WRITE_DONE)
    {
        LEDS_ON(BSP_LED_0_MASK);
        if (elara_flash_if_can_write_done())
        {
            elara_flash_write_done();
            gUAS = UPDATE_AVR_INIT_UART;
        }
        app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(1000, 0), NULL);
    }
    else if (gUAS == UPDATE_AVR_INIT_UART)
    {
        LEDS_OFF(BSP_LED_0_MASK);
        //avr_uart_init(UART_BAUDRATE_BAUDRATE_Baud115200);
        gUAS = UPDATE_AVR_INIT_AVR;
        app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(1000, 0), NULL);
    }
    else if (gUAS == UPDATE_AVR_INIT_AVR)
    {
        int rc = 0;
        LEDS_ON(BSP_LED_0_MASK);
        switch (gPartID)
        {
        case m328p_16M_5V:
        case m328p_8M_3_3V:
            rc = avr_init_common_1("m328p");
            break;
        default:
            rc = avr_init_common_1("m328p");
            break;
        }
        //open_LED(1);
        //open_LED(4);//Green
        if (rc < 0)
        {
            leda_save_error(rc);
            led_blink_error(BSP_LED_0_MASK);
            leda_leave_connect_to_debug(rc);
            return;
        }
        gUAS = UPDATE_AVR_STATE_2;
        app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(500, 0), NULL);
        
    }
    else if (gUAS == UPDATE_AVR_STATE_2)
    {
        LEDS_OFF(BSP_LED_0_MASK);
        int rc = avr_init_common_2();
        if (rc < 0)
        {
            leda_save_error(rc);
            led_blink_error(BSP_LED_0_MASK | BSP_LED_1_MASK);
            leda_leave_connect_to_debug(rc);
            return;
        }

        gUAS = UPDATE_AVR_STATE_3;
        app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(500, 0), NULL);
    }
    else if (gUAS == UPDATE_AVR_STATE_3)
    {
        exitRC = avr_write_elara_2_next();
        if (exitRC == -1)//Update AVR finished.
        {
            LEDS_ON(BSP_LED_0_MASK);
            avr_write_elara_2_finished();
            app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(1000, 0), NULL);
            gUAS = UPDATE_AVR_STATE_4;
            return;
        }
        else if (exitRC >= 0)
        {
            app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(100, 0), NULL);
        }
        else
        {
            leda_save_error(exitRC);
            led_blink_error(BSP_LED_2_MASK | BSP_LED_3_MASK);
            leda_leave_connect_to_debug(exitRC);
            return;
        }
        
    }
    else if (gUAS == UPDATE_AVR_STATE_4)
    {
        LEDS_OFF(BSP_LED_0_MASK);
        nrf_gpio_cfg_default(AVR_RESET_PIN);
        gUAS = UPDATE_AVR_STATE_5;
        app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(100, 0), NULL);
    }
    else if (gUAS == UPDATE_AVR_STATE_5)
    {
        LEDS_ON(BSP_LED_0_MASK);
        avr_uart_uninit();
        //avr_uart_rx_timeout_stop();
        close_LED(1);
        close_LED(2);

        gUAS = UPDATE_AVR_FINISHED;
        app_timer_start(m_avr_timer_id, APP_TIMER_TICKS(1000, 0), NULL);
    }
    else if (gUAS == UPDATE_AVR_FINISHED)
    {
        LEDS_OFF(BSP_LED_0_MASK);
        gUAS = UPDATE_AVR_IDLE;
#ifndef UPDATE_AVR_TEST
        strcpy((char*)g_pstorage_buffer, "Update done");
        ble_nus_string_send_bin_blcok(&m_nus, g_pstorage_buffer, 11);
#endif
        gLedaIsUpdattingAVR = false;
        leda_storage_update_BLEAVR();
    }
}

