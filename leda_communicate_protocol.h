
#ifndef __LEDA_COMMUNICATE_PROTOCOL_H__
#define __LEDA_COMMUNICATE_PROTOCOL_H__

#include "stdint.h"
//#include "bd_ble_nus.h"
#include "mimas_ble_nus.h"
#include "tethys_ble_nus.h"
#include "leda_ble_nus.h"

/******************* Macro defination *************************************/
#define L1_HEADER_MAGIC  (0xAB)     /*header magic number */
#define L1_HEADER_iBEACON_MAGIC (0xEA)
#define L1_HEADER_VERSION (0x00)     /*protocol version */
#define L1_HEADER_SIZE   (8)      /*L1 header length*/

#define MD_BLOCK_SIZE         16
/*****************************************/
/* time bit field */
typedef struct
{
uint32_t seconds  :
    6;
uint32_t minute  :
    6;
uint32_t hours  :
    5;
uint32_t day   :
    5;
uint32_t month  :
    4;
uint32_t year   :
    6;
}
time_bit_field_type_t;

typedef union
{
    uint32_t data;
    time_bit_field_type_t time;
} time_union_t;
/* time bit field */

/******************* Function defination **********************************/
void L1_receive_data_bin_size(ble_nus_t *p_nus, uint8_t *data, uint16_t length);
void L1_receive_data_bin_block(ble_nus_t *p_nus, uint8_t *data, uint16_t length);
void L1_receive_data_error_reset(ble_nus_t *p_nus, uint8_t *data, uint16_t length);
void L1_receive_data_uart(tethys_ble_nus_t *p_nus, uint8_t *data, uint16_t length);
void L1_leda_receive_data(leda_ble_nus_t *p_nus, uint8_t *data, uint16_t length);
void leda_tell_app_transfer_bin(uint32_t avSize);
void leda_uart_init(void);
void leda_init_communicate(void);
void leda_uart_uninit(void);
#endif //__LEDA_COMMUNICATE_PROTOCOL_H__
