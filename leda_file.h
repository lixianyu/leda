/* Copyright (c) [2014 Baidu]. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File Name          :
 * Author             :
 * Version            : $Revision:$
 * Date               : $Date:$
 * Description        :
 *
 * HISTORY:
 * Date               | Modification                    | Author
 * 28/03/2014         | Initial Revision                |

 */
#ifndef __LEDA_FILE_H__
#define __LEDA_FILE_H__

#include <stdbool.h>

/******************* Macro defination *************************************/
#define LEDA_FLASH_WORD_SIZE     4
#define LEDA_FLASH_SIZE          32768 // 32K bytes
/*****************************************/


/******************* Function defination **********************************/
extern void leda_pstorage_init(void);
extern void elara_flash_write_init(void);
extern int elara_flash_write(uint16_t len, uint8_t *pBuf);
extern int elara_flash_write_done(void);
extern bool elara_flash_if_can_write_done(void);
extern int elara_flash_read_init(void);
extern void elara_flash_read( uint16_t len, uint8_t *pBuf );
extern uint32_t elara_flash_if_flash_enough(uint32_t binSize);
extern int elara_flash_erase(void);
extern void elara_flash_lost_connected(void);
extern void leda_storage_update_baudrate(void);
extern void leda_storage_update_BLEAVR(void);
extern int32_t leda_storage_get_temp_offset(int32_t adcTemp);
extern void leda_storage_update_temp_offset(int32_t adcTemp, int32_t curTemp);
extern void leda_save_error(int errcode);
extern void leda_save_sys_run_time(void);
#endif //__LEDA_FILE_H__
